import argparse
from ConfigParser import ConfigParser
from datetime import date, timedelta, datetime as dt
import os
import pandas  as pd
import sys

# import pyspark
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--config', '-config', type=str, help='config file')
parser.add_argument('--sample',type=float,help='fraction of the order line data to extract, as a float between 0 and 1')
args = parser.parse_args()
if args.config is not None:
    config = ConfigParser()
    config.read(args.config)

    # PROJECT_DIR = config.get('paths', 'project')
    # OUTPUT_DIR = config.get('paths', 'output')
    SPARK_HOME = config.get('paths', 'spark')

    os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
    os.environ["SPARK_HOME"] = SPARK_HOME
    os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
    os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
    os.environ[
        'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 1 --driver-memory 8g --executor-memory 8g --executor-cores 2 --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'
    sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
    sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
    execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark.sparkContext.setLogLevel("ERROR")
    helios_order_header = (spark
                           .table("helios.helios_order_header_stage")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark
                        .table("helios.helios_order_line_stage")
                        .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))
else:
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark = (SparkSession.builder.master("local").appName("Customer Attr Compute")
             .getOrCreate())
    helios_order_header = (spark.read.format("csv").options(header="true")
                           .load("training_data/atrium_helios_order_header_sample.csv")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark.read.format("csv").options(header="true")
                         .load("training_data/atrium_helios_order_line_sample.csv")
                         .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    customer_profile = (spark.read.format("csv").options(header="true")
                        .load('training_data/atrium_customer_profile_sample.csv'))
    web_activity = (spark.read.format("csv").options(header="true")
                    .load('training_data/atrium_customer_web_activity.csv')
                    .withColumn('dt', f.substring('dt', 0, 10).cast('timestamp')))
    premium_membership = (spark.read.format("csv").options(header="true")
                          .load('training_data/atrium_premium_membership_sample.csv'))
    sku_to_category = (spark.read.format("csv").options(header="true")
                       .load("training_data/sku_to_category.csv"))


ONE_YEAR = 365
TODAY = dt.strptime('2018/05/13','%Y/%m/%d')

if args.sample is not None:
    helios_order_line = helios_order_line.sample(False,fraction=args.sample,seed=42)


def yoy(total, prior_total, first_purchase_date=None, today=None):
    """
    :param total: total current year spend
    :param prior_total: total prior year spend
    :param first_purchase_date: first purchase date in category
    :param today: date to use as today
    :return: total/prior_total if applicable
    """
    start_date = today - timedelta(2 * ONE_YEAR)
    if (prior_total is None) \
            or prior_total == 0 \
            or (first_purchase_date is None) \
            or (first_purchase_date >= start_date) \
            or (total is None):
        return None
    else:
        return float(total)/prior_total


yoy_udf = udf(yoy, FloatType())


def bt_2(total, recent, prior_total, prior_recent, first_purchase=None, today=None):
    """
    :param total: current year total
    :param recent: current year recent total
    :param prior_total: prior year total
    :param prior_recent: prior year recent (parallel time period) total
    :param first_purchase: first purchase date
    :param today: date to use as today
    :return: A value of 1 is stable: recent spend is stable with respect to yearly spend AND/OR in line with prior year
    behavior. If insufficient prior year data exists, only return twice ratio of current year to next year.
    If little or no activity, return None.
    """
    if (first_purchase is None) \
            or (first_purchase>today-timedelta(ONE_YEAR)) \
            or total == 0:
        return None
    elif prior_total == 0 \
            or prior_recent==0 \
            or (first_purchase > today-timedelta(ONE_YEAR * 2)):
        return 2*float(recent) / total  # <--1.0 is the stable amount, 2 is all sales in recent, 0 is no sales in recent
    else:
        return float((recent*prior_total)) / (prior_recent * total)


bt_2_udf = udf(bt_2, FloatType())


spend_by_cat = (helios_order_line
                .join(sku_to_category,
                    [helios_order_line.staples_sku_number == sku_to_category.sku_number])
                .select('customer_number', 'order_number', 'order_date', 'total_sale_price', 'primary_category')
                .filter(f.col('total_sale_price') > 0)
                .fillna({'primary_category': 'OTHER'})
                )

first_order_by_cat = (spend_by_cat
                      .groupBy('customer_number','primary_category')
                      .agg(f.min('order_date').alias('first')))

prior_total = (spend_by_cat
                        .filter(f.col('order_date') >= (TODAY-timedelta(2*ONE_YEAR)))
                        .filter(f.col('order_date') < (TODAY-timedelta(ONE_YEAR)))
                        .groupBy('customer_number','primary_category')
                        .agg(
                            f.sum('total_sale_price').alias('prior_year_spend')))
prior_recent = (spend_by_cat
                .filter(f.col('order_date') >= (TODAY-timedelta(1.5*ONE_YEAR)))
                .filter(f.col('order_date') < (TODAY-timedelta(ONE_YEAR)))
                .groupBy('customer_number', 'primary_category')
                .agg(
                    f.sum('total_sale_price').alias('prior_year_recent_spend')))
current_total = (spend_by_cat
                 .filter(f.col('order_date') >= (TODAY-timedelta(ONE_YEAR)))
                 .filter(f.col('order_date') < TODAY)
                 .groupBy('customer_number', 'primary_category')
                 .agg(
                    f.sum('total_sale_price').alias('current_year_spend')))
current_recent = (spend_by_cat
                  .filter(f.col('order_date') >= (TODAY-timedelta(0.5*ONE_YEAR)))
                  .filter(f.col('order_date') < TODAY)
                  .groupBy('customer_number', 'primary_category')
                  .agg(
                    f.sum('total_sale_price').alias('current_year_recent_spend')))
aggregates = (first_order_by_cat
                .join(prior_total, ['customer_number', 'primary_category'],how='outer')
                .join(prior_recent, ['customer_number', 'primary_category'],how='outer')
                .join(current_total, ['customer_number', 'primary_category'],how='outer')
                .join(current_recent, ['customer_number', 'primary_category'],how='outer')
                .fillna(0.0)
                .withColumn('today', f.lit(TODAY)))

categories = [col.primary_category for col in sku_to_category.select('primary_category').distinct().collect()]

cat_spend_aggregates_yoy = (aggregates
                    .withColumn('yoy',
                                yoy_udf('current_year_spend', 'prior_year_spend',
                                        'first', 'today')))

cat_spend_aggregates_yoy_pivot = (cat_spend_aggregates_yoy
                                  .groupBy('customer_number')
                                  .pivot('primary_category')
                                  .agg(f.sum('yoy')))

for name in categories:
    if name not in cat_spend_aggregates_yoy_pivot.columns:
        cat_spend_aggregates_yoy_pivot = (cat_spend_aggregates_yoy_pivot
                                          .withColumn(name+': Year-over-year spend',f.lit(None)))
    else:
        cat_spend_aggregates_yoy_pivot = (cat_spend_aggregates_yoy_pivot
                                          .withColumnRenamed(name,name + ': Year-over-year spend'))


total_spend_count = (current_total
                 .filter(f.col('current_year_spend') > 0)
                 .groupBy('customer_number')
                 .agg(f.sum('current_year_spend').alias('overall_current_year_spend'),
                      f.count('primary_category').alias('Number of categories purchased')))

cat_spend_aggregates_bench = (aggregates
                        .withColumn('benchmark trend',
                                    bt_2_udf('current_year_spend', 'current_year_recent_spend',
                                             'prior_year_spend', 'prior_year_recent_spend', 'first',
                                             'today')))
cat_spend_aggregates_bench_pivot = (cat_spend_aggregates_bench
                                    .groupBy('customer_number')
                                    .pivot('primary_category')
                                    .agg(f.sum('benchmark trend')))
for name in categories:
    if name not in cat_spend_aggregates_bench_pivot.columns:
        cat_spend_aggregates_bench_pivot = (cat_spend_aggregates_bench_pivot
                                            .withColumn(name+': Trend', f.lit(None)))
    else:
        cat_spend_aggregates_bench_pivot = (cat_spend_aggregates_bench_pivot
                                            .withColumnRenamed(name, name + ': Trend'))


spend_aggregates = (total_spend_count
                    .join(cat_spend_aggregates_yoy_pivot,'customer_number','outer')
                    .join(cat_spend_aggregates_bench_pivot,'customer_number','outer'))

spend_aggregates.repartition(1).toPandas().to_csv('spend_aggregates_june_26.csv')


spark.stop()
