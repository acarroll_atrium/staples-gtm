from ConfigParser import ConfigParser
import os
import sys

from datetime import date, timedelta, datetime as dt
import argparse

ONE_YEAR = 365

def return_ratio(num_orders_with_returns, num_orders):
    """
    :param num_orders_with_returns:
    :param num_orders:
    :return: ratio of number of orders with returns to total number of orders
    """
    if num_orders == 0:
        return None
    else:
        return num_orders_with_returns / num_orders


def cadence_statistics(df, start_date, end_date, name='', keyword_dict={}):
    """

    :param df: pyspark dataframe
    :param start_date: beginning of time
    :param end_date: end of time
    :param name: desired based name for columns
    :param keyword_dict: should include date column and partition column to indicate which columns of df are for dates
    and partitioning
    :return: Average lag between events, standard deviation, and total number of events
    """
    dateCol = keyword_dict['dateCol']
    partitionCol = keyword_dict['partitionCol']
    window = (Window
              .partitionBy(partitionCol)
              .orderBy(dateCol))
    df_filter = (df
                 .filter(df[dateCol] >= start_date)
                 .filter(df[dateCol] < end_date))
    df_filter = df_filter.select(partitionCol, dateCol).drop_duplicates()
    df_lagged = (df_filter.withColumn('lag', f.datediff(dateCol, f.lag(dateCol, 1).over(window))))
    return (df_lagged
        .groupBy(partitionCol)
        .agg(
        f.countDistinct('lag').alias(name + '_count'),
        f.mean('lag').alias(name + 'cadence_avg'),
        f.stddev('lag').alias(name + 'cadence_std')
    )
    )


def bt_2(total, recent, prior_total, prior_recent, first_purchase=None, today=None):
    """
    :param total: current year total
    :param recent: current year recent total
    :param prior_total: prior year total
    :param prior_recent: prior year recent (parallel time period) total
    :param first_purchase: first purchase date
    :param today: date to use as today
    :return: A value of 1 is stable: recent spend is stable with respect to yearly spend AND/OR in line with prior year
    behavior. If insufficient prior year data exists, only return twice ratio of current year to next year.
    If little or no activity, return None.
    """
    if not first_purchase or (first_purchase>today-timedelta(ONE_YEAR)) or total==0:
        return None
    elif prior_total==0 or prior_recent==0 or (first_purchase>today-timedelta(ONE_YEAR * 2)):
        return 2*recent / total # <--1.0 is the stable amount, 2 is all sales in recent, 0 is no sales in recent
    else:
        return (recent*prior_total) / (prior_recent * total)


def yoy(total, prior_total, first_purchase_date=None, today=None):
    """

    :param total: total current year spend
    :param prior_total: total prior year spend
    :param first_purchase_date: first purchase date in category
    :param today: date to use as today
    :return: total/prior_total if applicable
    """
    start_date = today - timedelta(2 * ONE_YEAR)
    if prior_total==0 or not prior_total or not first_purchase_date or (first_purchase_date >= start_date):
        return None
    else:
        return total/prior_total


def days_since_event(df, reference_date, dateCol, partitionCol, colLabel='event'):
    """

    :param df: dataframe
    :param reference_date: a date to reference
    :param dateCol: column in df that contains date
    :param partitionCol: column containing groupby feature
    :param colLabel: Name for output column
    :return: partitionColumn and date difference between most recent (pre-reference_date) event and reference date.
    """
    df_filter = (df
                 .filter(f.col(dateCol) < reference_date))
    return (df_filter
        .groupBy(partitionCol)
        .agg(f.max(dateCol).alias('most_recent_date'))
        .select(
        partitionCol,
        f.datediff(f.lit(reference_date), 'most_recent_date').alias('days_since_last_{}'.format(colLabel)))
    )


def cart_addition_detector(x):
    if len(x) > 0:
        for i in x:
            if i.startswith("12"):
                return 1
    return 0


def customer_attributes(helios_order_header,helios_order_line,
                        customer_profile,web_activity,premium_membership,
                        sku_to_category,training=False,today=dt.today()):
    return_ratio_udf = udf(return_ratio, FloatType())
    bt_2_udf = udf(bt_2, FloatType())
    yoy_udf = udf(yoy, FloatType())

    #####################
    ### Customer Age ####
    #####################
    customer_age = helios_order_header.groupBy('customer_number').agg(f.min('order_date').alias('first_order_date'))
    customer_age_df = (customer_age
                       .withColumn('customer_age', f.datediff(f.lit(today), 'first_order_date'))
                       .select('customer_number', 'customer_age'))

    #############################################
    # Spend by category                         #
    # - Percent in each                         #
    # - Trend benchmarked by last year          #
    # - Year-over-year ratio                    #
    # - Overall number of categories purchased  #
    #############################################

    spend_by_cat = (helios_order_line
        .join(sku_to_category,
            [helios_order_line.staples_sku_number == sku_to_category.sku_number])
        .select('customer_number', 'order_number', 'order_date', 'total_sale_price', 'primary_category')
        .filter(f.col('total_sale_price') > 0)
        .fillna({'primary_category': 'OTHER'})
        )

    first_order_by_cat = (spend_by_cat
                          .groupBy('customer_number','primary_category')
                          .agg(f.min('order_date').alias('first')))

    prior_total = (spend_by_cat
                            .filter(f.col('order_date')>=(today-timedelta(2*ONE_YEAR)))
                            .filter(f.col('order_date')<(today-timedelta(ONE_YEAR)))
                            .groupBy('customer_number','primary_category')
                            .agg(
                                f.sum('total_sale_price').alias('prior_year_spend')))
    prior_recent = (spend_by_cat
                    .filter(f.col('order_date')>=(today-timedelta(1.5*ONE_YEAR)))
                    .filter(f.col('order_date')<(today-timedelta(ONE_YEAR)))
                    .groupBy('customer_number','primary_category')
                    .agg(
                        f.sum('total_sale_price').alias('prior_year_recent_spend')))
    current_total = (spend_by_cat
                     .filter(f.col('order_date')>=(today-timedelta(ONE_YEAR)))
                     .filter(f.col('order_date')<(today))
                     .groupBy('customer_number','primary_category')
                     .agg(
                        f.sum('total_sale_price').alias('current_year_spend')))
    current_recent = (spend_by_cat
                      .filter(f.col('order_date')>=(today-timedelta(0.5*ONE_YEAR)))
                      .filter(f.col('order_date')<(today))
                      .groupBy('customer_number','primary_category')
                      .agg(
                        f.sum('total_sale_price').alias('current_year_recent_spend')))

    # totals_and_count = (current_total
    #                     .groupBy('customer_number')
    #                     .agg(f.sum('current_year_spend').alias('total spend'),f.countDistinct('primary_category')))
    cat_spend_aggregates = (first_order_by_cat
                    .join(prior_total,['customer_number','primary_category'],how='outer')
                    .join(prior_recent,['customer_number','primary_category'],how='outer')
                    .join(current_total,['customer_number','primary_category'],how='outer')
                    .join(current_recent,['customer_number','primary_category'],how='outer')
                    .fillna(0.0))
    print 'Cat Spend Agg '+str(cat_spend_aggregates.head())

    total_spend_count = (current_total
                         .filter(f.col('current_year_spend')>0)
                         .groupBy('customer_number')
                         .agg(f.sum('current_year_spend').alias('overall_current_year_spend'),
                              f.countDistinct('primary_category').alias('Number of categories purchased')))
    print 'Total spend ct '+str(total_spend_count.head())
    for column in ['current_year_spend','current_year_recent_spend','prior_year_spend','prior_year_recent_spend']:
        cat_spend_aggregates = cat_spend_aggregates.withColumn(column,f.col(column).cast(FloatType()))

    print 'Cat Agg Cast'+str(cat_spend_aggregates.head())


    cat_spend_aggregates = (cat_spend_aggregates
                        .withColumn('benchmark trend',
                                    bt_2_udf('current_year_spend','current_year_recent_spend',
                                            'prior_year_spend','prior_year_recent_spend','first',
                                             f.lit(today))))
    print 'Cat Agg Bench' +str(cat_spend_aggregates.head())

    cat_spend_aggregates = (cat_spend_aggregates
                        .withColumn('yoy',
                                    yoy_udf('current_year_spend','prior_year_spend',
                                            'first',f.lit(today))))
    print 'Cat Agg YOY' + str(cat_spend_aggregates.head())

    cat_spend_with_pct = (cat_spend_aggregates
                            .join(total_spend_count.select('customer_number','overall_current_year_spend'),
                                  'customer_number',
                                  'outer')
                            .withColumn('spend percent',
                                        f.col('current_year_spend')/f.col('overall_current_year_spend')))
    print 'Cat Spend: PCT '+str(cat_spend_with_pct.head())

    try:
        cat_spend_pivot = (cat_spend_aggregates
            .groupBy('customer_number')
            .pivot('primary_category')
            .agg(
                f.first('yoy').alias('Year-over-year'),
                f.first('bench trend').alias('Trend')))
    except:
        cat_spend_pivot_bench = (cat_spend_aggregates
                                 .groupBy('customer_number')
                                 .pivot('primary_category')
                                 .sum('benchmark trend').alias('Trend'))
        print 'Cat Spend: Bench ' + str(cat_spend_pivot_bench.head())

        cat_spend_pivot_yoy = (cat_spend_aggregates
                               .groupBy('customer_number')
                               .pivot('primary_category')
                               .sum('yoy').alias('Year-over-year'))
        print 'Cat Spend: YOY ' + str(cat_spend_pivot_yoy.head())

        cat_spend_pivot = (cat_spend_pivot_bench
                           .join(cat_spend_pivot_yoy, 'customer_number', 'outer'))

    print 'Cat Spend join ' + str(cat_spend_pivot.head())
    cat_spend_pivot_df = (cat_spend_pivot.join(total_spend_count.select('customer_number',
                                                                        'Number of categories purchased',
                                                                        f.col('overall_current_year_spend').alias(
                                                                            'Current Year Spend')),
                                               ['customer_number'],
                                               'outer'))
    print 'Cat Spend join '+str(cat_spend_pivot_df.head())

    #cat_col_rename = [f.col(c).alias(c.replace('_','Spend: ') if c != 'customer_number' else c) for c in cat_spend_pivot.columns]
    #cat_spend_pivot_df = (cat_spend_pivot
    #                        .select(cat_col_rename))


    mm_customer_list = (customer_age_df
                        .join(cat_spend_pivot_df,'customer_number','outer'))

    return mm_customer_list

if __name__=='__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--training', action='store_true', default=False,
                        help='flag to indicate this run will extract training data')
    parser.add_argument('--today', type=str)
    parser.add_argument('--config', '-config', type=str, help='config file')
    parser.add_argument('--sample',action='store_true',default=False)
    args = parser.parse_args()

    if args.training:
        if args.today:
            TODAY = dt.strptime(args.today, '%Y/%m/%d')
        else:
            TODAY = dt.today() - timedelta(ONE_YEAR)
    else:
        TODAY = dt.today()

    if args.config:

        config = ConfigParser()
        config.read(args.config)

        # PROJECT_DIR = config.get('paths', 'project')
        # OUTPUT_DIR = config.get('paths', 'output')
        SPARK_HOME = config.get('paths', 'spark')

        os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
        os.environ["SPARK_HOME"] = SPARK_HOME
        os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
        os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
        os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
        os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
        os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
        os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
        os.environ[
            'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 48 --driver-memory 8g --executor-memory 8g --executor-cores 2 --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'
        sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
        sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
        execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
        #spark.sparkContext.addPyFile(config.get('paths', 'timewindows'))
        from pyspark.sql.functions import udf
        from pyspark.sql import functions as f
        from pyspark.sql.types import *
        from pyspark.sql.window import Window
        from pyspark.sql import SparkSession

        spark.sparkContext.setLogLevel("ERROR")


        helios_order_header = (spark
                                   .table("helios.helios_order_header_stage")
                                   .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        helios_order_line = (spark
                             .table("helios.helios_order_line_stage")
                             .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))
        web_activity = spark.table("helios.customer_web_activity")
        customer_profile = spark.table('helios.helios_customer_profile_stage')
        premium_membership = spark.table('helios.premium_lifetime_savings')
        if args.sample:
            helios_order_header = helios_order_header.sample(False,fraction=0.05,seed=42)
            web_activity = web_activity.sample(False,fraction=0.05,seed=42)
            helios_order_line = helios_order_line.sample(False,fraction=0.05,seed=42)
    else:
        from pyspark.sql.functions import udf
        from pyspark.sql import functions as f
        from pyspark.sql.types import *
        from pyspark.sql.window import Window
        from pyspark.sql import SparkSession
        spark = (SparkSession.builder.master("local").appName("Customer Attr Compute")
                    .getOrCreate())
        helios_order_header = (spark.read.format("csv").options(header="true")
                               .load("training_data/atrium_helios_order_header_sample.csv")
                               .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        helios_order_line = (spark.read.format("csv").options(header="true")
                             .load("training_data/atrium_helios_order_line_sample.csv")
                             .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        customer_profile = (spark.read.format("csv").options(header="true")
                            .load('training_data/atrium_customer_profile_sample.csv'))
        web_activity = (spark.read.format("csv").options(header="true")
                        .load('training_data/atrium_customer_web_activity.csv')
                        .withColumn('dt', f.substring('dt', 0, 10).cast('timestamp')))
        premium_membership = (spark.read.format("csv").options(header="true")
                              .load('training_data/atrium_premium_membership_sample.csv'))
        sku_to_category = (spark.read.format("csv").options(header="true")
                           .load("training_data/sku_to_category.csv"))

    cust_attr = customer_attributes(helios_order_header, helios_order_line,
                                    customer_profile, web_activity, premium_membership,
                                    sku_to_category,training=args.training,today=TODAY)
    try:
        if args.training:
            path='customer_spend_{}.csv'.format(str(TODAY)[:10])
        else:
            path='customer_spend.csv'
        cust_attr.write.csv(path=path,header="true",mode="overwrite",sep="\t")
    except:
        cust_attr.toPandas().to_csv(path)
    spark.stop()
