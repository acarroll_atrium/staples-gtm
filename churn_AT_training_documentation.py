#!/usr/bin/env python
# coding: utf-8

# # Setup and configuration
# First cell is not for running in jupyter notebook, but instead running the file churn_AT_training.py script.
# 
# Key Notes:
# 1. Three optional arguments: training, today, config
#   - training defaults to false if unspecified; if true, data is written to two .csv files: train and test. Furthermore, "today" defaults to one year ago, or can be specified;
#   - today is utilized only in training runs, in which the end date for consideration of training data is set by this parameter. It shoudl take the form YYYY/mm/DD
#   - config references a config file which should contain paths for 'project', 'output', and 'spark'
# 2. Java home for cluster, spark home, python path, and pyspark submit args are all specified here

# In[ ]:


#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create Spark Env
import argparse
from ConfigParser import ConfigParser
from datetime import date, timedelta, datetime as dt
import os
import pandas  as pd
import sys

parser = argparse.ArgumentParser()

parser.add_argument('--training', action='store_true',
                    default=False,
                    help='Flag indicating this run will extract training data, with target column, as a .csv file')
parser.add_argument('--today', type=str, help='Aggregates are computed over 12 month period ending on this day. Format YYYY/mm/DD')
parser.add_argument('--config', '-config', type=str, help='config file')

args = parser.parse_args()

ONE_YEAR = 365
if args.training:
    try:
        today = dt.strptime(args.today, '%Y/%m/%d')
    except (ValueError, TypeError) as err:
        today = dt.strptime(str(dt.today())[:10], '%Y-%m-%d') - timedelta(ONE_YEAR)
        print err
else:
    today = dt.strptime(str(dt.today())[:10], '%Y-%m-%d')

config = ConfigParser()
config.read(args.config)
#
PROJECT_DIR = config.get('paths', 'project')
OUTPUT_DIR = config.get('paths', 'output')
SPARK_HOME = config.get('paths', 'spark')

os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
os.environ["SPARK_HOME"] = SPARK_HOME
os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
os.environ[
    'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 48 --driver-memory 8g --executor-memory 8g --executor-cores 2 pyspark-shell'
sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
spark.sparkContext.setLogLevel("ERROR")


# # Test
# This cell remains so that Yue Zhang can perform experiements in his own environment.

# In[ ]:


####################
# Reserve for Yue  #
####################


# # import sys
# # sys.path.append('/home/hadoop/datashdp/shared/RewriteZaphodModel/')
# #
# # import Models.model_utils
# # import PythonUtils.s3_model_management
# # from cluster_tools import context_creator
# # import pandas
# # spark_exec_script = context_creator.spark_executable('2.1.1', 'BN', num_exec=48, exec_mem='8g',
# #                                                      exec_cores=2, driver_mem='8g')
# #
# # execfile(spark_exec_script, globals())
#
# from pyspark.sql import functions as f
# from pyspark.sql.window import Window
# from datetime import date, timedelta, datetime as dt
#
# spark.sparkContext.setLogLevel("ERROR")


# # Function definitions
# Herein, four functions are defined:
# 1. cadence_statistics
# > Given a partition column and a date column, computes the average elapsed time between events per group, standard deviation, and total number of events.
# 2. yoy_pd
# > Given a _pandas_ dataframe that contains columns `current_year_spend` and `prior_year_spend`, computes the ratio of current_year_spend to prior_year_spend.
# 3. bt_2_pd
# > Given a _pandas_ dataframe with 'current_year_spend', 'prior_year_spend', 'current_year_recent_spend', 'prior_year_recent_spend' columns, computes the ratio of 'current_year_recent_spend' to 'prior_year_recent_spend'. This is assumed to be recent 6 month spend vs. full 12 month spend, so stable spending indicates this ratio would be approximately 1/2. The result of this function is one of two quantities:
# > - Twice the above ratio, _unless_ prior_year_recent_spend is non-null;
# > - In that case, the ratio is divided by 'prior_year_recent_spend / 'prior_year_spend'. This should be near 1 if the prior year spend trend is roughly consistent with that of this year. 
# 4. days_since_event
# > This function computes the most recent event which was prior to a given reference date in each group. The grouping is provided by a partition column. 

# In[ ]:


def cadence_statistics(df, start_date, end_date, date_col, partition_col, name=''):
    """
    Cadence statistics groups the dataframe by a partition column, ordering by date,
    then computes the total number of events per group, average time delta per group,
    and standard deviation for this number, per group.
    :param df: pyspark dataframe
    :param start_date: beginning of time
    :param end_date: end of time
    :param name: desired based name for columns
    :param date_col: column to use for dates
    :param partition_col: column to use for groupby
    :return: Average lag between events, standard deviation, and total number of events
    """
    window = (Window
              .partitionBy(partition_col)
              .orderBy(date_col))
    df_filter = (df
                 .filter(df[date_col] >= start_date)
                 .filter(df[date_col] < end_date))
    df_filter = df_filter.select(partition_col, date_col).drop_duplicates()
    df_lagged = (df_filter.withColumn('lag', f.datediff(date_col, f.lag(date_col, 1).over(window))))
    return (df_lagged
        .groupBy(partition_col)
        .agg(
        f.count(date_col).alias(name + '_count'),
        f.mean('lag').alias(name + '_cadence_avg'),
        f.stddev('lag').alias(name + '_cadence_std')
    )
    )


def yoy_pd(row):
    """
    The yoy_pd function takes a dataframe with columns labeled 'current_year_spend',
    'prior_year-spend' and returns the ratio of total to prior. To afford for current_year_spend
    that is of any size with prior year spend equal to zero, one is added to the denominator of the ratio.
    The order of of magnitude of sales by category makes this irrelevant.

    :param total: total current year spend
    :param prior_total: total prior year spend
    :param first_purchase_date: first purchase date in category
    :param today: date to use as today
    :return: total/prior_total if applicable
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    try:
        return float(total) / (prior_total + 1)
    except:
        return 0.0


def bt_2_pd(row):
    """
    bt_2_pd (bench-trend) computes the following ratios: recent ratio = current_year_recent_spend / current_year_spend,
    and prior ratio = prior_year_recent_spend / prior_year_spend. The recent ratio will be near 1/2 if spending is
    stable across a one-year period. This ratio is divided by prior ratio, when possible. In this case, a score
    of 1 indicates stable spend in a category when compared against spend in the same category last year.
    If the prior ratio is unavailable, the reported ratio is twice the recent ratio (so that 1 indicates stable
    spend across a one-year period).
    :param total: current year total
    :param recent: current year recent total
    :param prior_total: prior year total
    :param prior_recent: prior year recent (parallel time period) total
    :param first_purchase: first purchase date
    :param today: date to use as today
    :return: A value of 1 is stable: recent spend is stable with respect to yearly spend AND/OR in line with prior year
    behavior. If insufficient prior year data exists, only return twice ratio of current year to next year.
    If little or no activity, return None.
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    recent = row['current_year_recent_spend']
    prior_recent = row['prior_year_recent_spend']
    today = row['TODAY']
    first_purchase_date = row['first']
    if total == 0 or (first_purchase_date is None):
        return 0
    elif prior_total == 0 or prior_recent == 0 or (first_purchase_date > today - timedelta(ONE_YEAR * 2)):
        return 2 * float(recent) / total
    else:
        return float(recent * prior_total) / ((prior_recent + 1) * (total + 1))


# def num_std_past(num, avg, std):
#     try:
#         if num >= avg:
#             return float(num-avg)/std
#         else:
#      return 0.0
#     except:
#         return 0.0
# num_std_past_udf = udf(num_std_past, FloatType())

def days_since_event(df, reference_date, date_col, partition_col, col_label='event'):
    """
    The dataframe df is grouped by the partition column, and the time difference between the reference_date
    and the largest date_col date prior to reference date is returned, for each group).
    :param df: dataframe
    :param reference_date: a date to reference
    :param date_col: column in df that contains date
    :param partition_col: column containing groupby feature
    :param col_label: Name for output column
    :return: partition_column and date difference between most recent (pre-reference_date) event and reference date.
    """
    df_filter = (df
                 .filter(f.col(date_col) < reference_date))
    return (df_filter
        .groupBy(partition_col)
        .agg(f.max(date_col).alias('most_recent_date'))
        .select(
        partition_col,
        f.datediff(f.lit(reference_date), 'most_recent_date').alias('days_since_last_{}'.format(col_label)))
    )


# # Table imports
# At most 6 tables are touched by this script.
# 1. Helios order header: information about the buyer, date, total and type of each order made.
# 2. Helios order line: detail about each order, by line item, including order number, sku, and price.
# 3. Sku to category: A lookup table between sku numbers and primary product category.
# 4. Customer web activity: A selection of details about cart-add data.
# 5. Customer Profile: A daily update of certain account fields from the Salesforce table
# 6. Premium membership: Information about savings accrued during the premium membership phase of a customer's lifetime.

# In[ ]:


helios_order_header = (spark
                       .table("helios.helios_order_header_stage")
                       .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))

helios_order_line = (spark
                     .table("helios.helios_order_line_stage")
                     .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))

sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))

# web_activity = spark.table("helios.customer_web_activity").sample(False,0.05, 24)

customer_profile = spark.table('helios.helios_customer_profile_stage')

premium_membership = spark.table('helios.premium_lifetime_savings')


# # Customer Age
# Customer age is computed by taking the difference between today and the firt order date. 

# In[ ]:


customer_age = (helios_order_header
                .groupBy('customer_number')
                .agg(f.min('order_date').alias('first_order_date')))

customer_age_df = (customer_age
                   .withColumn('customer_age', f.datediff(f.lit(today), 'first_order_date'))
                   .select('customer_number', 'customer_age'))

mm_customer_list = customer_age_df


# # Web Activity Cadence
# Web activity is originally intended to compute total number of web visits over a one year period and the ratio of cart adds to total purchases, also over one year. Even the code from the in-production churn_run.py code fails here, and so it was witheld. 

# In[ ]:


#########################
# Web activity          #
#########################
# web_activity_filt = (web_activity
#                      .where(f.col("dt").isNotNull())
#                      .filter(f.col('dt') > today - timedelta(ONE_YEAR))
#                      .filter(f.col('dt') <= today))

# web_activity_dates = (web_activity.select('cust_nmb', 'dt').drop_duplicates())
#
#
# web_visit_cadence_df = (cadence_statistics(web_activity_dates, today - timedelta(ONE_YEAR),
#                                            today, 'dt', 'cust_nmb', name='web_activity')
#                         .withColumnRenamed('cust_nmb', 'customer_number'))
#
#
# days_since_last_web_visit_df = (days_since_event(web_activity_dates, today, 'dt', 'cust_nmb', 'web_visit')
#                                 .withColumnRenamed('cust_nmb', 'customer_number'))
#
# def cart_addition_detector(x):
#     if len(x)>0:
#         for i in x:
#             if i .startswith("12"):
#                 return 1
#     return 0

# fun4 = udf(cart_addition_detector, IntegerType())
#
# web_activity_df = (web_activity.filter(web_activity['dt']>today-timedelta(ONE_YEAR)).filter(web_activity['dt']<today))
#
# web_activity_df = (web_activity_df.where(f.col('event_list_string').like('%,%')).withColumn('event_list_substring', f.regexp_replace('event_list_string',',','|')).where(f.length('event_list_substring')>0))
#
# web_activity_agg_df = (web_activity_df.groupBy("cust_nmb","user_id","dt").agg(f.collect_list(web_activity_df.event_list_substring).alias("list_of_events")).cache())
#
# web_activity_agg_df = web_activity_agg_df.repartition(1)
#
# web_activity_agg= web_activity_agg_df.withColumn("cart_add_indicator", fun4("list_of_events"))
#
# web_activity_agg = web_activity_agg.groupBy('cust_nmb').agg(f.count('dt').alias('total_online_visits'), f.sum('cart_add_indicator').alias('cart_activities'))
#
# web_activity_agg = web_activity_agg.select('cust_nmb','total_online_visits', (web_activity_agg.cart_activities/web_activity_agg.total_online_visits).alias('cart_add_ratio'))
# web_activity_agg = web_activity_agg.withColumnRenamed('cust_nmb','customer_number')
#
# mm_customer_list = mm_customer_list.join(web_activity_agg,'customer_number','left')


# # Lifetime savings
# Aggregate the lts column in the premium_membership table. 

# In[ ]:


#############################
# Lifetime Savings          #
#############################

premium_membership_df = (premium_membership
                         .withColumn('premium_enrolled', f.lit(1))
                         .select('customer_number',
                                 'premium_enrolled',
                                 premium_membership.lts.cast('float').alias('lts')))

premium_membership_df = (premium_membership_df.groupBy('customer_number', 'premium_enrolled')
                         .agg(f.sum('lts').alias('lts')))

mm_customer_list = mm_customer_list.join(premium_membership_df, 'customer_number', 'left')


# # Twelve-month Spend
# These cells focus on order within the prior 12 months. Computed are the following quantities:
# 1. number_of_orders
# > Count of the distinct order numbers in the order header (with positive order amounts that are not ajustments)
# 2. twelve_month_rolling_sales
# > Total sales, measured by adding the order amounts in the same order header (with positive order amounts that are not ajustments)
# 3. number_of_orders_with_returns
# > Compute number of distinct order numbers per customer_number in which there is a negative order amount
# 4. number_of_returned_items
# > Compute the number of returned items as those from the order_line table with negative sale price.
# 5. number_of_items_purchased
# > Compute number of items purchased as those from order_line with positive sale price.
# 6. number_of_distinct_items_purchased
# > Count distinct skus purchased
# 7. ratio_of_returned_items
# > Ratio of (3) to (1)

# In[ ]:


#############################################
# Twelve-month spend                        #
# Twelve-month number of orders             #
# Twelve-month number of items purchased    #
#############################################

helios_order_header_current = (helios_order_header
                               .filter(f.col('order_date') < today)
                               .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR))))

orders = (helios_order_header_current
            .filter(f.col('order_type') == 'OR')
            .filter(f.col('order_amount') > 0)
            .groupBy('customer_number')
            .agg(
                f.countDistinct('order_number').alias('number_of_orders'),
                f.sum('order_amount').alias('twelve_month_rolling_sales')))

returns = (helios_order_header_current
           .filter(f.col('order_amount') < 0)
           .groupBy('customer_number')
           .agg(f.countDistinct('order_number').alias('number_of_orders_with_returns'))
           )

returned_items = (helios_order_line
                  .filter(f.col('order_date') < today)
                  .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
                  .filter(f.col('total_sale_price') < 0)
                  .groupBy('customer_number')
                  .agg(f.count('order_date').alias('number_of_returned_items'))
                  )

purchased_items = (helios_order_line
    .filter(f.col('order_date') < today)
    .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
    .filter(f.col('total_sale_price') > 0)
    .groupBy('customer_number')
    .agg(
    f.count('order_date').alias('number_of_items_purchased'),
    f.countDistinct('staples_sku_number').alias('number_of_distinct_items_purchased'))
)

orders_df = (orders
             .join(returns, ['customer_number'], how='outer')
             .join(returned_items, ['customer_number'], how='outer')
             .join(purchased_items, ['customer_number'], how='outer')
             .fillna(0.0))

orders_df = (orders_df
             .select(
    'customer_number',
    'number_of_returned_items',
    'number_of_orders',
    'number_of_items_purchased',
    'number_of_distinct_items_purchased',
    'twelve_month_rolling_sales',
    'number_of_orders_with_returns')
             .withColumn('ratio_of_returned_items', f.col('number_of_orders_with_returns') / f.col('number_of_orders'))
             )

mm_customer_list = mm_customer_list.join(orders_df, 'customer_number', 'inner')


# # Purchase trend
# Computes the average rate at which a customer tends to buy over a 2 year span of time. This computes time between purchases for a 2 year period, computs the mean and standard deviation.

# In[ ]:


#############################
# Purchase trend            #
#############################

positive_orders = helios_order_header.filter(f.col('order_type') == 'OR').filter(f.col('order_amount') > 0)

purchase_cadence_df = cadence_statistics(positive_orders, today - timedelta(2 * ONE_YEAR),
                                         today, 'order_date', 'customer_number', name='purchase')

mm_customer_list = mm_customer_list.join(purchase_cadence_df, 'customer_number', 'outer')

days_since_last_order_df = days_since_event(positive_orders, today, 'order_date', 'customer_number', 'order')

mm_customer_list = mm_customer_list.join(days_since_last_order_df, 'customer_number', 'outer')

pre_past_purchase_cadence = (f.col('days_since_last_order') - f.col('purchase_cadence_avg')) / f.col(
    'purchase_cadence_std')
mm_customer_list = mm_customer_list.withColumn('pre_past_purchase_cadence', pre_past_purchase_cadence)

past_purchase_cadence = f.when(f.col('pre_past_purchase_cadence') < 0, 0).otherwise(f.col('pre_past_purchase_cadence'))
mm_customer_list = mm_customer_list.withColumn('past_purchase_cadence', past_purchase_cadence)


# # Spend by category
# The result of cat_spend_pivot_df computes year-over-year and benchmark-trend (as defined by the functions in the "Function Definitions" cell) for each primary category purchased by each customer. For memory issues, the year-over-year and benchmark-trend couldn't be applied as UDFs (Yue Zhang may investigate). Therefore, a work-around was decided in which the spend-by-customer-number-and-category aggregate is written to a Pandas dataframe, the functions are applied to the dataframe, it is pivoted, written to a .csv file locally, written to HDFS, then read back in as a spark dataframe. 

# In[ ]:


#############################
# Spend by category         #
#############################


spend_by_cat = (helios_order_line
                .join(sku_to_category,
                      [helios_order_line.staples_sku_number == sku_to_category.sku_number])
                .select('customer_number', 'order_number', 'customer_item_no', 'order_date',
                        'shipped_sku_number', 'total_sale_price', 'primary_category')
                .filter(f.col('total_sale_price') > 0)
                .dropna(subset=['shipped_sku_number'])
                .fillna({'primary_category': 'OTHER'}))

first_order_by_cat = (spend_by_cat
                      .groupBy('customer_number', 'primary_category')
                      .agg(f.min('order_date').alias('first')))

prior_total = (spend_by_cat
    .filter(f.col('order_date') >= (TODAY - timedelta(2 * ONE_YEAR)))
    .filter(f.col('order_date') < (TODAY - timedelta(ONE_YEAR)))
    .groupBy('customer_number', 'primary_category')
    .agg(
    f.sum('total_sale_price').alias('prior_year_spend')))

prior_recent = (spend_by_cat
    .filter(f.col('order_date') >= (TODAY - timedelta(1.5 * ONE_YEAR)))
    .filter(f.col('order_date') < (TODAY - timedelta(ONE_YEAR)))
    .groupBy('customer_number', 'primary_category')
    .agg(
    f.sum('total_sale_price').alias('prior_year_recent_spend')))

current_total = (spend_by_cat
    .filter(f.col('order_date') >= (TODAY - timedelta(ONE_YEAR)))
    .filter(f.col('order_date') < TODAY)
    .groupBy('customer_number', 'primary_category')
    .agg(
    f.sum('total_sale_price').alias('current_year_spend')))

current_recent = (spend_by_cat
    .filter(f.col('order_date') >= (TODAY - timedelta(0.5 * ONE_YEAR)))
    .filter(f.col('order_date') < TODAY)
    .groupBy('customer_number', 'primary_category')
    .agg(
    f.sum('total_sale_price').alias('current_year_recent_spend')))

aggregates = (first_order_by_cat
              .join(prior_total, ['customer_number', 'primary_category'], how='outer')
              .join(prior_recent, ['customer_number', 'primary_category'], how='outer')
              .join(current_total, ['customer_number', 'primary_category'], how='outer')
              .join(current_recent, ['customer_number', 'primary_category'], how='outer')
              .fillna(0.0)
              .withColumn('TODAY', f.lit(TODAY)))

cat_spend_agg_df = aggregates.toPandas()
cat_spend_agg_df['yoy'] = cat_spend_agg_df.apply(yoy_pd, axis=1)
cat_spend_agg_df['bench'] = cat_spend_agg_df.apply(bt_2_pd, axis=1)
cat_spend_pivot = cat_spend_agg_df.pivot_table(index='customer_number', columns='primary_category',
                                               values=['yoy', 'bench'])
new_cols = ['customer_number']
for x in zip(cat_spend_pivot.columns.get_level_values(0),
             [str(name) for name in cat_spend_pivot.columns.get_level_values(1)]):
    if x[0] == 'yoy':
        new_cols.append('{}_spend_year_over_year'.format(x[1].lower().replace(' ', '_')))
    else:
        new_cols.append('{}_spend_trend'.format(x[1].lower().replace(' ', '_')))

cat_spend_pivot = cat_spend_pivot.reset_index()
cat_spend_pivot.columns = new_cols

cat_spend_pivot.to_csv('/tmp/cat_spend_2019_07_16.csv', index=False)

os.system("hadoop fs -put -f /tmp/cat_spend_2019_07_16.csv")

cat_spend_pivot_df = spark.read.format("csv").option("header", "true").option("inferSchema", "true").load(
    "/tmp/cat_spend_2019_07_16.csv")

mm_customer_list = mm_customer_list.join(cat_spend_pivot_df, 'customer_number', 'left')


# # Number of Categories
# The dataframe `current_total` is grouped by customer_number and primary_category. A customer-number-primary-category pair occurs in this dataframe if and only if the given customer purchased said category in the recent 12 months. Therefore, the groupBy customer_number followed by count yields the number of distinct categories purchased.

# In[ ]:


no_categories = current_total.groupBy('customer_number').agg(
    f.count('primary_category').alias('no_of_categories_purchased'))

mm_customer_list = mm_customer_list.join(no_categories, 'customer_number', 'left')


# # Training Data
# If the data is to be extracted for training purposes, aggregates on the time _past_ today need to be taken, including number of orders in the subsequent 12 months as well as the total spend. all_orders_df computes the marking for logistic regression: next-year-sales to this-year-sales ratio at least 60% and at least 50% as many orders is considered a non-churn, all others are churners.
# 
# IF args.training is true, this target column is computed, all customers with at least 7 orders over the past 12 months are extracted, and a 60-40 split of the data is created and written to two separate .csv files.
# 
# OTHERWISE the fields on which the model was trained are extracted, and written to the Hive table `hawkeye.sa_churn_data_for_einstein`

# In[ ]:


#############################
# Extract Training          #
#############################
if args.training:
    helios_order_header_future = (helios_order_header
                                  .filter(f.col('order_date') >= today)
                                  .filter(f.col('order_date') < (today + timedelta(today))))

    orders_future = (helios_order_header_future
        .filter(f.col('order_type') == 'OR')
        .filter(f.col('order_amount') > 0)
        .groupBy('customer_number')
        .agg(
        f.countDistinct('order_number').alias('future_orders'),
        f.sum('order_amount').alias('future_spend')))

    all_orders_df = (orders_future.select('customer_number', 'future_orders', 'future_spend')
                     .join(mm_customer_list.select('customer_number', 'number_of_orders', 'twelve_month_rolling_sales'),
                           ['customer_number'], how='outer')
                     .withColumn('sales_ratio', f.col('future_spend') / f.col('twelve_month_rolling_sales'))
                     .withColumn('orders_ratio', f.col('future_orders') / f.col('number_of_orders'))
                     .withColumn('target',
                                 f.when((f.col('sales_ratio') >= 0.6) & (f.col('orders_ratio') >= 0.5), 0).otherwise(1))
                     .fillna(0.0)
                     .select('customer_number', 'future_orders', 'future_spend', 'target'))

    mm_customer_list = (mm_customer_list
                        .filter(f.col('number_of_orders') > 6)
                        .join(all_orders_df, 'customer_number', 'left'))

    mm_customer_list_split = mm_customer_list.randomSplit([0.6, 0.4])

    mm_customer_list_split[0].toPandas().to_csv('/tmp/customer_churn_attributes_train_{}.csv'.format(str(today)[:10]))
    mm_customer_list_split[1].toPandas().to_csv('/tmp/customer_churn_attributes_test_{}.csv'.format(str(today)[:10]))

else:
    mm_customer_list = (mm_customer_list
                    .withColumnRenamed('ship_&_pack_spend_trend', 'ship_pack_spend_trend')
                    .withColumnRenamed('generalist_tech_spend_year_over_year', 'generalist_year_over_year')
                    .withColumnRenamed('project_furniture_spend_trend', 'furniture_spend_trend')
                    .withColumnRenamed('project_furniture_spend_year_over_year', 'furniture_year_over_year')
                    .withColumnRenamed('ship_&_pack_spend_year_over_year', 'ship_pack_year_over_year')
                    .withColumnRenamed('specialist_tech_spend_year_over_year', 'specialist_spend_year_over_year')
                    .select('customer_number', 'customer_age', 'lts', 'number_of_returned_items', 'number_of_orders',
                            'number_of_items_purchased', 'number_of_orders_with_returns', 'purchase_cadence_avg',
                            'purchase_cadence_std', 'past_purchase_cadence',
                            'days_since_last_order', 'breakroom_spend_trend', 'generalist_tech_spend_trend',
                            'jansan_spend_trend',
                            'office_supplies_spend_trend', 'paper_spend_trend', 'print_spend_trend',
                            'specialist_tech_spend_trend',
                            'toner_spend_trend', 'breakroom_spend_year_over_year', 'jansan_spend_year_over_year',
                            'office_supplies_spend_year_over_year',
                            'paper_spend_year_over_year', 'print_spend_year_over_year', 'ship_pack_spend_trend',
                            'generalist_year_over_year',
                            'furniture_year_over_year', 'furniture_spend_trend', 'ship_pack_year_over_year', 'specialist_spend_year_over_year',
                            'no_of_categories_purchased', 'twelve_month_rolling_sales')
                    .withColumn('cart_add_ratio', f.lit(0.0))
                    .withColumn('total_online_visits', f.lit(0.0)))

    mm_customer_list.write.mode("overwrite").saveAsTable("hawkeye.sa_churn_data_for_einstein")


# In[ ]:


spark.stop()

