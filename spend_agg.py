import argparse
from ConfigParser import ConfigParser
from datetime import date, timedelta, datetime as dt
import os
import pandas as pd
import sys

# import pyspark
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--config', '-config', type=str, help='config file')
parser.add_argument('--sample',action='store_true',default=False)
args = parser.parse_args()
if args.config is not None:
    config = ConfigParser()
    config.read(args.config)

    # PROJECT_DIR = config.get('paths', 'project')
    # OUTPUT_DIR = config.get('paths', 'output')
    SPARK_HOME = config.get('paths', 'spark')

    os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
    os.environ["SPARK_HOME"] = SPARK_HOME
    os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
    os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
    os.environ[
        'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 1 --driver-memory 8g --executor-memory 8g --executor-cores 2 --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'
    sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
    sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
    execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark.sparkContext.setLogLevel("ERROR")
    helios_order_header = (spark
                           .table("helios.helios_order_header_stage")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark
                         .table("helios.helios_order_line_stage")
                         .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp'))
                         .withColumnRenamed('staples_sku_number','sku_number'))
    sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))
    web_activity = spark.table("helios.customer_web_activity")
    customer_profile = spark.table('helios.helios_customer_profile_stage')
    premium_membership = spark.table('helios.premium_lifetime_savings')
else:
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark = (SparkSession.builder.master("local").appName("Customer Attr Compute")
             .getOrCreate())
    helios_order_header = (spark.read.format("csv").options(header="true")
                           .load("training_data/atrium_helios_order_header_sample.csv")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark.read.format("csv").options(header="true")
                         .load("training_data/atrium_helios_order_line_fixed.csv")
                         .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp'))
                         .withColumnRenamed('staples_sku_number','sku_number'))
    customer_profile = (spark.read.format("csv").options(header="true")
                        .load('training_data/atrium_customer_profile_sample.csv'))
    web_activity = (spark.read.format("csv").options(header="true")
                    .load('training_data/atrium_customer_web_activity.csv')
                    .withColumn('dt', f.substring('dt', 0, 10).cast('timestamp')))
    premium_membership = (spark.read.format("csv").options(header="true")
                          .load('training_data/atrium_premium_membership_sample.csv'))
    sku_to_category = (spark.read.format("csv").options(header="true")
                       .load("training_data/sku_to_category.csv"))


ONE_YEAR = 365
TODAY = dt.strptime('2018/05/13','%Y/%m/%d')
today = TODAY

if args.sample is not None:
    helios_order_line = helios_order_line.sample(False,fraction=0.1,seed=42)


def yoy(total, prior_total, first_purchase_date=None, today=None):
    """
    :param total: total current year spend
    :param prior_total: total prior year spend
    :param first_purchase_date: first purchase date in category
    :param today: date to use as today
    :return: total/prior_total if applicable
    """
    start_date = today - timedelta(2 * ONE_YEAR)
    if prior_total==0 or (prior_total is None) or (first_purchase_date is None) or (first_purchase_date >= start_date) or (total is None):
        return None
    else:
        return float(total)/prior_total


def bt_2(total, recent, prior_total, prior_recent, first_purchase=None, today=None):
    """
    :param total: current year total
    :param recent: current year recent total
    :param prior_total: prior year total
    :param prior_recent: prior year recent (parallel time period) total
    :param first_purchase: first purchase date
    :param today: date to use as today
    :return: A value of 1 is stable: recent spend is stable with respect to yearly spend AND/OR in line with prior year
    behavior. If insufficient prior year data exists, only return twice ratio of current year to next year.
    If little or no activity, return None.
    """
    if (first_purchase is None) or (first_purchase>today-timedelta(ONE_YEAR)) or total==0:
        return None
    elif prior_total == 0 or prior_recent == 0 or (first_purchase>today-timedelta(ONE_YEAR * 2)):
        return 2*float(recent) / total  # <--1.0 is the stable amount, 2 is all sales in recent, 0 is no sales in recent
    else:
        return float((recent*prior_total)) / (prior_recent * total)


def cart_add_detector(code):
    if len(code) > 0:
        if code.startswith("12"):
            return 1
    return 0


def return_ratio(num_orders_with_returns, num_orders):
    """
    :param num_orders_with_returns:
    :param num_orders:
    :return: ratio of number of orders with returns to total number of orders
    """
    if num_orders is None or num_orders == 0:
        return None
    else:
        return num_orders_with_returns / num_orders


def days_since_event(df, reference_date, date_col, partition_col, col_label='event'):
    """

    :param df: dataframe
    :param reference_date: a date to reference
    :param date_col: column in df that contains date
    :param partition_col: column containing groupby feature
    :param col_label: Name for output column
    :return: partition_column and date difference between most recent (pre-reference_date) event and reference date.
    """
    df_filter = (df
                 .filter(f.col(date_col) < reference_date))
    return (df_filter
        .groupBy(partition_col)
        .agg(f.max(date_col).alias('most_recent_date'))
        .select(partition_col, f.datediff(f.lit(reference_date),
                                          'most_recent_date').alias('days_since_last_{}'.format(col_label))))


def cadence_statistics(df, start_date, end_date, name='', keyword_dict={}):
    """

    :param df: pyspark dataframe
    :param start_date: beginning of time
    :param end_date: end of time
    :param name: desired based name for columns
    :param keyword_dict: should include date column and partition column to indicate which columns of df are for dates
    and partitioning
    :return: Average lag between events, standard deviation, and total number of events
    """
    date_col = keyword_dict['date_col']
    partition_col = keyword_dict['partition_col']
    window = (Window
              .partitionBy(partition_col)
              .orderBy(date_col))
    df_filter = (df
                 .filter(df[date_col] >= start_date)
                 .filter(df[date_col] < end_date))
    df_filter = df_filter.select(partition_col, date_col).drop_duplicates()
    df_lagged = (df_filter.withColumn('lag', f.datediff(date_col, f.lag(date_col, 1).over(window))))
    return (df_lagged
        .groupBy(partition_col)
        .agg(
        f.count(date_col).alias(name + '_count'),
        f.mean('lag').alias(name + 'cadence_avg'),
        f.stddev('lag').alias(name + 'cadence_std')
    )
    )


return_ratio_udf = udf(return_ratio, FloatType())
bt_2_udf = udf(bt_2, FloatType())
yoy_udf = udf(yoy, FloatType())
cart_add_udf = udf(cart_add_detector, IntegerType())


ONE_YEAR = 365
TODAY = dt.strptime('2018/05/13', '%Y/%m/%d')
today = TODAY


try:
    customer_age = helios_order_header.groupBy('customer_number').agg(f.min('order_date').alias('first_order_date'))
    customer_age_df = (customer_age
                       .withColumn('customer_age', f.datediff(f.lit(today), 'first_order_date'))
                       .select('customer_number', 'customer_age'))
    customer_age_df.head()
except:
    print 'Customer Age Error'

############################
#   YOY and seasonal spend #
############################

spend_by_cat = (helios_order_line
    .join(sku_to_category,'sku_number','left')
    .select('customer_number', 'order_number', 'order_date', 'total_sale_price', 'primary_category')
    .filter(f.col('total_sale_price') > 0)
    .fillna({'primary_category': 'OTHER'})
    )

first_order_by_cat = (spend_by_cat
                      .groupBy('customer_number','primary_category')
                      .agg(f.min('order_date').alias('first')))

prior_total = (spend_by_cat
                .filter(f.col('order_date')>=(TODAY-timedelta(2*ONE_YEAR)))
                .filter(f.col('order_date')<(TODAY-timedelta(ONE_YEAR)))
                .groupBy('customer_number','primary_category')
                .agg(
                    f.sum('total_sale_price').alias('prior_year_spend')))
prior_recent = (spend_by_cat
                .filter(f.col('order_date')>=(TODAY-timedelta(1.5*ONE_YEAR)))
                .filter(f.col('order_date')<(TODAY-timedelta(ONE_YEAR)))
                .groupBy('customer_number','primary_category')
                .agg(
                    f.sum('total_sale_price').alias('prior_year_recent_spend')))
current_total = (spend_by_cat
                 .filter(f.col('order_date') >= (TODAY-timedelta(ONE_YEAR)))
                 .filter(f.col('order_date') < (TODAY))
                 .groupBy('customer_number','primary_category')
                 .agg(
                    f.sum('total_sale_price').alias('current_year_spend')))
current_recent = (spend_by_cat
                  .filter(f.col('order_date')>=(TODAY-timedelta(0.5*ONE_YEAR)))
                  .filter(f.col('order_date')<(TODAY))
                  .groupBy('customer_number','primary_category')
                  .agg(
                    f.sum('total_sale_price').alias('current_year_recent_spend')))
aggregates = (first_order_by_cat
                .join(prior_total, ['customer_number', 'primary_category'], how='outer')
                .join(prior_recent, ['customer_number', 'primary_category'], how='outer')
                .join(current_total, ['customer_number', 'primary_category'], how='outer')
                .join(current_recent, ['customer_number', 'primary_category'], how='outer')
                .fillna(0.0)
                .withColumn('today', f.lit(TODAY)))

categories = [col.primary_category for col in sku_to_category.select('primary_category').distinct().collect()]

try:
    print aggregates.count()
    print 'Aggregates created with the above number of rows'
except:
    print 'Aggregates failed'

try:
    cat_spend_aggregates_yoy = (aggregates
                        .withColumn('yoy',
                                    yoy_udf('current_year_spend', 'prior_year_spend',
                                            'first', 'today')))

    cat_spend_aggregates_yoy_pivot = (cat_spend_aggregates_yoy
                                      .groupBy('customer_number')
                                      .pivot('primary_category')
                                      .agg(f.sum('yoy').alias('yoy')))
    for name in categories:
        if name not in cat_spend_aggregates_yoy_pivot.columns:
            cat_spend_aggregates_yoy_pivot = (cat_spend_aggregates_yoy_pivot
                                              .withColumn(name+': Year-over-year spend',f.lit(None)))
        else:
            cat_spend_aggregates_yoy_pivot = (cat_spend_aggregates_yoy_pivot
                                              .withColumnRenamed(name,name + ': Year-over-year spend'))
    cat_spend_aggregates_yoy_pivot.count()
    print 'Yoy pivot created with the above number of rows'
except:
    print 'Yoy failure'

try:
    total_spend_count = (current_total
                         .filter(f.col('current_year_spend') > 0)
                         .groupBy('customer_number')
                         .agg(
                            f.sum('current_year_spend').alias('overall_current_year_spend'),
                            f.countDistinct('primary_category').alias('Number of categories purchased')))
    total_spend_count.count()
    print 'Total spend written with the above number of rows'
except:
    print 'Totals failure'

try:
    cat_spend_aggregates_bench = (aggregates
                            .withColumn('benchmark trend',
                                        bt_2_udf('current_year_spend', 'current_year_recent_spend',
                                                 'prior_year_spend', 'prior_year_recent_spend', 'first',
                                                 'today')))

    cat_spend_aggregates_bench.count()

    print 'Bench trend written with the above number of rows'
    cat_spend_aggregates_bench_pivot = (cat_spend_aggregates_bench
                                        .groupBy('customer_number')
                                        .pivot('primary_category')
                                        .agg(f.sum('benchmark trend')))
    for name in categories:
        if name not in cat_spend_aggregates_bench_pivot.columns:
            cat_spend_aggregates_bench_pivot = (cat_spend_aggregates_bench_pivot
                                                .withColumn(name+': Trend', f.lit(None)))
        else:
            cat_spend_aggregates_bench_pivot = (cat_spend_aggregates_bench_pivot
                                                .withColumnRenamed(name, name + ': Trend'))
    cat_spend_aggregates_bench_pivot.count()
    print 'Bench pivots written with the above number of rows'
except:
    print 'Bench Trend Failure'

#####################
# Cart Add Ratio    #
#####################
try:
    web_activity_agg = (web_activity
                        .filter(f.col('dt') > today - timedelta(ONE_YEAR))
                        .filter(f.col('dt') <= today))

    # Web activity cadence
    web_activity_dates = (web_activity.select('cust_nmb', 'dt').drop_duplicates())

    web_visit_cadence_df = (cadence_statistics(web_activity_dates,
                                               today - timedelta(ONE_YEAR),
                                               today,
                                               name='web_activity',
                                               keyword_dict=dict([('date_col', 'dt'), ('partition_col', 'cust_nmb')]))
                            .withColumnRenamed('cust_nmb', 'customer_number'))

    days_since_last_web_visit_df = (days_since_event(web_activity_dates, today, 'dt', 'cust_nmb', 'web_visit')
                                    .withColumnRenamed('cust_nmb', 'customer_number'))
    web_activity_totals = (web_activity_agg
        .withColumn('cart_add_indicator',cart_add_udf('event_list_string'))
        .groupBy("cust_nmb")
        .agg(
            f.count('dt').alias('total_online_visits'),
            f.sum('cart_add_indicator').alias('cart_activities')
        )
    )


    def cart_add_computation(activities,visits):
        if (activities is not None) and (visits is not None) and (visits!=0):
            return float(activities)/visits
        else:
            return None
    cart_add_computation_udf = udf(cart_add_computation,FloatType())

    web_activity_agg_df = (web_activity_totals
                           .select(
        f.col('cust_nmb').alias('customer_number'),
        'total_online_visits',
        cart_add_computation_udf('cart_activities','total_online_visits').alias('cart_add_ratio')))

    (web_activity_agg_df
     .join(web_visit_cadence_df, 'customer_number', 'fullouter')
     .join(days_since_last_web_visit_df, 'customer_number', 'fullouter')
     .count())
    print 'Web activity written with the above number of rows'
except:
    print 'Web activity failed'


try:
    premium_membership_df = (premium_membership
                             .withColumn('premium_enrolled',f.lit(1))
                             .select('customer_number',
                                     'premium_enrolled',
                                     premium_membership.lts.cast('float').alias('lts')))
    premium_membership_df = premium_membership_df.groupBy('customer_number', 'premium_enrolled').agg(
        f.sum('lts').alias('lts'))
    premium_membership_df.count()
    print 'Premium Membership Written with the above number of rows'

except:
    print 'Premium Membership Failure'

#############################################
# Twelve-month spend                        #
# Twelve-month number of orders             #
# Twelve-month number of items purchased    #
#############################################
try:
    helios_order_header_current = (helios_order_header
                                   .filter(f.col('order_date') < today)
                                   .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR))))
    orders = (helios_order_header_current
        .filter(f.col('order_type') == 'OR')
        .filter(f.col('order_amount') > 0)
        .groupBy('customer_number')
        .agg(
        f.countDistinct('order_number').alias('Twelve-month number of orders'),
        f.sum('order_amount').alias('Twelve-month spend')
    ))
    returns = (helios_order_header_current
               .filter(f.col('order_amount') < 0)
               .groupBy('customer_number')
               .agg(f.countDistinct('order_number').alias('Number of orders with returns'))
               )
    returned_items = (helios_order_line
                      .filter(f.col('order_date') < today)
                      .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
                      .filter(f.col('total_sale_price') < 0)
                      .groupBy('customer_number')
                      .agg(f.count('order_date').alias('Number of returned items'))
                      )
    purchased_items = (helios_order_line
        .filter(f.col('order_date') < today)
        .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
        .filter(f.col('total_sale_price') > 0)
        .groupBy('customer_number')
        .agg(
        f.count('order_date').alias('Number of items purchased'),
        f.countDistinct('staples_sku_number').alias('Number of distinct items purchased')
    ))
    orders_df = (orders
                 .join(returns, ['customer_number'], how='outer')
                 .join(returned_items, ['customer_number'], how='outer')
                 .join(purchased_items, ['customer_number'], how='outer'))

    orders_df = (orders_df
        .fillna(0.0)
        .select(
        'customer_number',
        'Number of returned items',
        'Twelve-month number of orders',
        'Number of items purchased',
        'Number of distinct items purchased',
        'Twelve-month spend',
        return_ratio_udf('Number of orders with returns', 'Twelve-month number of orders').alias('Percent of orders with returns')
    ))
    orders_df.count()
    print 'Order Data with above number of rows'
except:
    print 'Orders DF Failure'


#############################
# Purchase trend            #
#############################
try:
    purchase_cadence_df = cadence_statistics(
        helios_order_header.where(f.col('order_type') == 'OR').where(f.col('order_amount') > 0),
        today - timedelta(2 * ONE_YEAR),
        today,
        name='purchase',
        keyword_dict=dict([('date_col', 'order_date'), ('partition_col', 'customer_number')])
    )

    days_since_last_order_df = days_since_event(
        helios_order_header.where(f.col('order_type') == 'OR').where(f.col('order_amount') > 0),
        today,
        'order_date',
        'customer_number',
        'order')

    (purchase_cadence_df
        .join(days_since_last_order_df,'customer_number','outer')
        .count())
    print 'Purchase Cadence with above number of rows'
except:
    print 'Purchase cadence failure'
# try:
#     cat_spend_aggregates.write.csv(path='spend_by_category_aggregates.csv',
#                            header="true", mode="overwrite", sep="\t")
# except:
#     cat_spend_aggregates.head().to_csv('spend_by_category_aggregates.csv')

##########################
#   Churn Marking        #
##########################

try:
    helios_order_header_future = (helios_order_header
                                  .filter(f.col('order_date') >= today)
                                  .filter(f.col('order_date') < (today + timedelta(ONE_YEAR))))
    orders_future = (helios_order_header_future
        .filter(f.col('order_type') == 'OR')
        .filter(f.col('order_amount') > 0)
        .groupBy('customer_number')
        .agg(
        f.countDistinct('order_number').alias('Number of orders future'),
        f.sum('order_amount').alias('Twelve-month spend future')
    ))


    def marking_strategy(current_spend, next_year_spend, num_orders_future, num_orders):
        try:
            return ((next_year_spend < 0.6 * current_spend) or (num_orders_future <= 0.5 * num_orders))
        except:
            return True


    marking_udf = udf(marking_strategy, BooleanType())

    all_orders = (orders_future
                  .join(orders, ['customer_number'], how='outer')
                  .fillna(0.0)
                  .withColumn('target', marking_udf('Twelve-month spend', 'Twelve-month spend future',
                                                    'Number of orders future', 'Twelve-month number of orders')))
    all_orders.count()
    print 'Marking Strategy computed with above number of rows'
except:
    print 'Marking strategy failure'

spark.stop()