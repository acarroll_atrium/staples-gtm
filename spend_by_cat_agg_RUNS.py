import argparse
from ConfigParser import ConfigParser
from datetime import date, timedelta, datetime as dt
import os
import pandas  as pd
import sys

# import pyspark
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--config', '-config', type=str, help='config file')
args = parser.parse_args()

config = ConfigParser()
config.read(args.config)

# PROJECT_DIR = config.get('paths', 'project')
# OUTPUT_DIR = config.get('paths', 'output')
SPARK_HOME = config.get('paths', 'spark')

os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
os.environ["SPARK_HOME"] = SPARK_HOME
os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
os.environ[
    'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 1 --driver-memory 8g --executor-memory 8g --executor-cores 2 --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'
sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
#spark.sparkContext.addPyFile(config.get('paths', 'timewindows'))
spark.sparkContext.setLogLevel("ERROR")


import pyspark.sql.functions as f
from pyspark.sql.functions import udf  # , lit, datediff, countDistinct, count,
from pyspark.sql.types import *

ONE_YEAR = 365
TODAY = dt.strptime('2018/05/13','%Y/%m/%d')


helios_order_header = (spark
                           .table("helios.helios_order_header_stage")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
helios_order_line = (spark
                     .table("helios.helios_order_line_stage")
                     .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))

ONE_YEAR = 365
TODAY = dt.strptime('2018/05/13','%Y/%m/%d')

# YOY Spend ratio by category and benchmarked trend: assumes there exists at lookup table going from staples_SKU to category
spend_by_cat = (helios_order_line
    .join(sku_to_category,
        [helios_order_line.staples_sku_number == sku_to_category.sku_number])
    .select('customer_number', 'order_number', 'order_date', 'total_sale_price', 'primary_category')
    .filter(f.col('total_sale_price') > 0)
    .fillna({'primary_category': 'OTHER'})
    )

first_order_by_cat = (spend_by_cat
                      .groupBy('customer_number','primary_category')
                      .agg(f.min('order_date').alias('first')))

prior_total = (spend_by_cat
                        .filter(f.col('order_date')>=(TODAY-timedelta(2*ONE_YEAR)))
                        .filter(f.col('order_date')<(TODAY-timedelta(ONE_YEAR)))
                        .groupBy('customer_number','primary_category')
                        .agg(
                            f.sum('total_sale_price').alias('prior_year_spend')))
prior_recent = (spend_by_cat
                .filter(f.col('order_date')>=(TODAY-timedelta(1.5*ONE_YEAR)))
                .filter(f.col('order_date')<(TODAY-timedelta(ONE_YEAR)))
                .groupBy('customer_number','primary_category')
                .agg(
                    f.sum('total_sale_price').alias('prior_year_recent_spend')))
current_total = (spend_by_cat
                 .filter(f.col('order_date')>=(TODAY-timedelta(ONE_YEAR)))
                 .filter(f.col('order_date')<(TODAY))
                 .groupBy('customer_number','primary_category')
                 .agg(
                    f.sum('total_sale_price').alias('current_year_spend')))
current_recent = (spend_by_cat
                  .filter(f.col('order_date')>=(TODAY-timedelta(0.5*ONE_YEAR)))
                  .filter(f.col('order_date')<(TODAY))
                  .groupBy('customer_number','primary_category')
                  .agg(
                    f.sum('total_sale_price').alias('current_year_recent_spend')))
aggregates = (first_order_by_cat
                .join(prior_total,['customer_number','primary_category'],how='outer')
                .join(prior_recent,['customer_number','primary_category'],how='outer')
                .join(current_total,['customer_number','primary_category'],how='outer')
                .join(current_recent,['customer_number','primary_category'],how='outer'))

try:
    aggregates.write.csv(path='spend_by_category_aggregates.csv',
                           header="true", mode="overwrite", sep="\t")
except:
    aggregates.toPandas().to_csv('spend_by_category_aggregates.csv')

spark.stop()