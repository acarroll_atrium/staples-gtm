
import argparse
from ConfigParser import ConfigParser
from datetime import date, timedelta, datetime as dt
import os
import pandas  as pd
import sys

# import pyspark
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--config', '-config', type=str, help='config file')
parser.add_argument('--sample',type=float,help='fraction of the order line data to extract, as a float between 0 and 1')
args = parser.parse_args()
if args.config is not None:
    config = ConfigParser()
    config.read(args.config)

    # PROJECT_DIR = config.get('paths', 'project')
    # OUTPUT_DIR = config.get('paths', 'output')
    SPARK_HOME = config.get('paths', 'spark')

    os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
    os.environ["SPARK_HOME"] = SPARK_HOME
    os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
    os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
    os.environ[
        'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 1 --driver-memory 8g --executor-memory 8g --executor-cores 2 --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'
    sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
    sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
    execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark.sparkContext.setLogLevel("ERROR")
    helios_order_header = (spark
                           .table("helios.helios_order_header_stage")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark
                        .table("helios.helios_order_line_stage")
                        .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))
else:
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark = (SparkSession.builder.master("local").appName("Customer Attr Compute")
             .getOrCreate())
    helios_order_header = (spark.read.format("csv").options(header="true")
                           .load("training_data/atrium_helios_order_header_sample.csv")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark.read.format("csv").options(header="true")
                         .load("training_data/atrium_helios_order_line_fixed.csv")
                         .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    customer_profile = (spark.read.format("csv").options(header="true")
                        .load('training_data/atrium_customer_profile_sample.csv'))
    web_activity = (spark.read.format("csv").options(header="true")
                    .load('training_data/atrium_customer_web_activity.csv')
                    .withColumn('dt', f.substring('dt', 0, 10).cast('timestamp')))
    premium_membership = (spark.read.format("csv").options(header="true")
                          .load('training_data/atrium_premium_membership_sample.csv'))
    sku_to_category = (spark.read.format("csv").options(header="true")
                       .load("training_data/sku_to_category.csv"))


ONE_YEAR = 365
TODAY = dt.strptime('2018/05/13','%Y/%m/%d')

if args.sample is not None:
    helios_order_line = helios_order_line.sample(False,fraction=args.sample,seed=42)



def return_ratio(num_orders_with_returns, num_orders):
    """
    :param num_orders_with_returns:
    :param num_orders:
    :return: ratio of number of orders with returns to total number of orders
    """
    if (num_orders is None) or (num_orders == 0):
        return None
    else:
        return float(num_orders_with_returns) / num_orders


return_ratio_udf = udf(return_ratio, FloatType())

helios_order_header_current = (helios_order_header
                               .filter(f.col('order_date') < TODAY)
                               .filter(f.col('order_date') >= (TODAY - timedelta(ONE_YEAR))))
orders = (helios_order_header_current
    .filter(f.col('order_type') == 'OR')
    .filter(f.col('order_amount') > 0)
    .groupBy('customer_number')
    .agg(
    f.countDistinct('order_number').alias('number_of_orders'),
    f.sum('order_amount').alias('twelve_month_rolling_sales')))

returns = (helios_order_header_current
           .filter(f.col('order_amount') < 0)
           .groupBy('customer_number')
           .agg(f.countDistinct('order_number').alias('Number of orders with returns'))
           )

helios_order_line_current = helios_order_line.filter(f.col('order_date') < TODAY).filter(
    f.col('order_date') >= (TODAY - timedelta(ONE_YEAR)))

returned_items = (helios_order_line_current
                  .filter(f.col('total_sale_price') < 0)
                  .groupBy('customer_number')
                  .agg(f.count('order_date').alias('Number of returned items')))

purchased_items = (helios_order_line_current
    .filter(f.col('total_sale_price') > 0)
    .groupBy('customer_number')
    .agg(
    f.count('order_date').alias('number_of_items_purchased'),
))

orders_df = (orders
             .join(returns, ['customer_number'], how='outer')
             .join(returned_items, ['customer_number'], how='outer')
             .join(purchased_items, ['customer_number'], how='outer')
             .fillna(0)
             .withColumn('ratio_of_returned_items',
                         return_ratio_udf('Number of orders with returns', 'number_of_orders')))

orders_df.printSchema()


#orders_df.coalesce(1).write.format('org.apache.spark.sql.execution.datasources.csv.CSVFileFormat').mode('overwrite').option("header", "true").csv('/tmp/orders_june_27.csv')
orders_df.toPandas().to_csv('/tmp/orders_june_27.csv',encoding='utf-8')
spark.stop()

