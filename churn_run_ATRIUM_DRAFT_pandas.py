from ConfigParser import ConfigParser
import os
import sys

from datetime import date, timedelta, datetime as dt
import argparse

ONE_YEAR = 365

def return_ratio(num_orders_with_returns, num_orders):
    """
    :param num_orders_with_returns:
    :param num_orders:
    :return: ratio of number of orders with returns to total number of orders
    """
    if num_orders == 0:
        return 0
    else:
        return float(num_orders_with_returns) / num_orders


def cadence_statistics(df, start_date, end_date, date_col, partition_col, name=''):
    """

    :param df: pyspark dataframe
    :param start_date: beginning of time
    :param end_date: end of time
    :param name: desired based name for columns
    :param date_col: column to use for dates
    :param partition_col: column to use for groupby
    :return: Average lag between events, standard deviation, and total number of events
    """
    window = (Window
              .partitionBy(partition_col)
              .orderBy(date_col))
    df_filter = (df
                 .filter(df[date_col] >= start_date)
                 .filter(df[date_col] < end_date))
    df_filter = df_filter.select(partition_col, date_col).drop_duplicates()
    df_lagged = (df_filter.withColumn('lag', f.datediff(date_col, f.lag(date_col, 1).over(window))))
    return (df_lagged
        .groupBy(partition_col)
        .agg(
        f.count(date_col).alias(name + '_count'),
        f.mean('lag').alias(name + '_cadence_avg'),
        f.stddev('lag').alias(name + '_cadence_std')
    )
    )

def yoy_pd(row):
    """
    :param total: total current year spend
    :param prior_total: total prior year spend
    :param first_purchase_date: first purchase date in category
    :param today: date to use as today
    :return: total/prior_total if applicable
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    today = row['TODAY']
    first_purchase_date = row['first']
    start_date = today - timedelta(2 * ONE_YEAR)
    try:
        return float(total)/(prior_total+1)
    except:
        return 0


def bt_2_pd(row):
    """
    :param total: current year total
    :param recent: current year recent total
    :param prior_total: prior year total
    :param prior_recent: prior year recent (parallel time period) total
    :param first_purchase: first purchase date
    :param today: date to use as today
    :return: A value of 1 is stable: recent spend is stable with respect to yearly spend AND/OR in line with prior year
    behavior. If insufficient prior year data exists, only return twice ratio of current year to next year.
    If little or no activity, return None.
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    recent = row['current_year_recent_spend']
    prior_recent = row['prior_year_recent_spend']
    today = row['TODAY']
    first_purchase_date = row['first']
    if total==0 or (first_purchase_date is None):
        return 0
    elif prior_total==0 or prior_recent==0 or (first_purchase_date>today-timedelta(ONE_YEAR * 2)):
        return 2*float(recent) / total
    else:
        return float((recent*prior_total)) / ((prior_recent+1) * (total+1))

def num_std_past(num,avg,std):
    if not avg or std==0:
        return None
    if num<avg:
        return 0
    else:
        return float((num-avg))/std


def days_since_event(df, reference_date, date_col, partition_col, col_label='event'):
    """

    :param df: dataframe
    :param reference_date: a date to reference
    :param date_col: column in df that contains date
    :param partition_col: column containing groupby feature
    :param col_label: Name for output column
    :return: partition_column and date difference between most recent (pre-reference_date) event and reference date.
    """
    df_filter = (df
                 .filter(f.col(date_col) < reference_date))
    return (df_filter
        .groupBy(partition_col)
        .agg(f.max(date_col).alias('most_recent_date'))
        .select(
            partition_col,
            f.datediff(f.lit(reference_date), 'most_recent_date').alias('days_since_last_{}'.format(col_label)))
    )

def cart_add_detector(code):
    if len(code)>0:
        if code.startswith("12"):
            return 1
    return 0


def customer_attributes(helios_order_header, helios_order_line,
                        customer_profile, web_activity,premium_membership,
                        sku_to_category, training=False, today=dt.today()):
    return_ratio_udf = udf(return_ratio, FloatType())
    cart_add_udf = udf(cart_add_detector, IntegerType())
    num_std_past_udf = udf(num_std_past)
    #####################
    ### Customer Age ####
    #####################

    customer_age = (helios_order_header
                    .groupBy('customer_number')
                    .agg(f.min('order_date').alias('first_order_date')))

    customer_age_df = (customer_age
                       .withColumn('customer_age', f.datediff(f.lit(today), 'first_order_date'))
                       .select('customer_number', 'customer_age'))


    #####################
    # Cart Add Ratio    #
    #####################

    web_activity_filt = (web_activity
                         .filter(f.col('dt') > today - timedelta(ONE_YEAR))
                         .filter(f.col('dt') <= today))

    # Web activity cadence
    web_activity_dates = (web_activity.select('cust_nmb', 'dt').drop_duplicates())

    web_visit_cadence_df = (cadence_statistics(web_activity_dates, today - timedelta(ONE_YEAR),
                                               today, 'dt', 'cust_nmb', name='web_activity')
                            .withColumnRenamed('cust_nmb', 'customer_number'))

    days_since_last_web_visit_df = (days_since_event(web_activity_dates, today, 'dt', 'cust_nmb', 'web_visit')
                                    .withColumnRenamed('cust_nmb', 'customer_number'))
    web_activity_totals = (web_activity_filt
        .withColumn('cart_add_indicator', cart_add_udf('event_list_string'))
        .groupBy("cust_nmb")
        .agg(
            f.count('dt').alias('total_online_visits'),
            f.sum('cart_add_indicator').alias('cart_activities'))
        )

    web_activity_agg = (web_activity_totals
                           .select(
        f.col('cust_nmb').alias('customer_number'),
        'total_online_visits',
        (web_activity_totals.cart_activities / web_activity_totals.total_online_visits).alias('cart_add_ratio')))

    web_activity_agg_df = (web_activity_agg
                             .join(web_visit_cadence_df,'customer_number','fullouter')
                             .join(days_since_last_web_visit_df,'customer_number','fullouter'))


    #############################################
    # Spend by category                         #
    # - Percent in each                         #
    # - Trend benchmarked by last year          #
    # - Year-over-year ratio                    #
    # - Overall number of categories purchased  #
    #############################################

    spend_by_cat = (helios_order_line
                    .join(sku_to_category,
                          [helios_order_line.staples_sku_number == sku_to_category.sku_number])
                    .select('customer_number', 'order_number', 'order_date', 'total_sale_price', 'primary_category')
                    .filter(f.col('total_sale_price') > 0)
                    .fillna({'primary_category': 'OTHER'})
                    )

    first_order_by_cat = (spend_by_cat
                          .groupBy('customer_number','primary_category')
                          .agg(f.min('order_date').alias('first')))

    prior_total = (spend_by_cat
        .filter(f.col('order_date') >= (today-timedelta(2*ONE_YEAR)))
        .filter(f.col('order_date') < (today-timedelta(ONE_YEAR)))
        .groupBy('customer_number', 'primary_category')
        .agg(
        f.sum('total_sale_price').alias('prior_year_spend')))
    prior_recent = (spend_by_cat
        .filter(f.col('order_date') >= (today-timedelta(1.5*ONE_YEAR)))
        .filter(f.col('order_date') < (today-timedelta(ONE_YEAR)))
        .groupBy('customer_number', 'primary_category')
        .agg(
        f.sum('total_sale_price').alias('prior_year_recent_spend')))
    current_total = (spend_by_cat
        .filter(f.col('order_date') >= (today-timedelta(ONE_YEAR)))
        .filter(f.col('order_date') < (today))
        .groupBy('customer_number', 'primary_category')
        .agg(
        f.sum('total_sale_price').alias('current_year_spend')))
    current_recent = (spend_by_cat
        .filter(f.col('order_date') >= (today-timedelta(0.5*ONE_YEAR)))
        .filter(f.col('order_date') < (today))
        .groupBy('customer_number', 'primary_category')
        .agg(
        f.sum('total_sale_price').alias('current_year_recent_spend')))

    # totals_and_count = (current_total
    #                     .groupBy('customer_number')
    #                     .agg(f.sum('current_year_spend').alias('total spend'),f.countDistinct('primary_category')))
    cat_spend_aggregates = (first_order_by_cat
                            .join(prior_total,['customer_number','primary_category'],how='outer')
                            .join(prior_recent,['customer_number','primary_category'],how='outer')
                            .join(current_total,['customer_number','primary_category'],how='outer')
                            .join(current_recent,['customer_number','primary_category'],how='outer')
                            .fillna(0.0)
                            .withColumn('TODAY',f.lit(today)))

    total_spend_count_df = (current_total
                         .filter(f.col('current_year_spend')>0)
                         .groupBy('customer_number')
                         .agg(f.sum('current_year_spend').alias('overall_current_year_spend'),
                              f.countDistinct('primary_category').alias('Number of categories purchased')))

    # for column in ['current_year_spend','current_year_recent_spend','prior_year_spend','prior_year_recent_spend']:
    #     cat_spend_aggregates = cat_spend_aggregates.withColumn(column,f.col(column).cast(FloatType()))

    # bench_args = ['current_year_spend',
    #               'current_year_recent_spend',
    #               'prior_year_spend',
    #               'prior_year_recent_spend','first',f.lit(today)]
    # yoy_args = ['current_year_spend','prior_year_spend','first',f.lit(today)]

    cat_spend_aggregates_df = cat_spend_aggregates.toPandas()

    cat_spend_aggregates_df['yoy'] = cat_spend_aggregates_df.apply(yoy_pd,axis=1)
    cat_spend_aggregates_df['trend'] = cat_spend_aggregates_df.apply(bt_2_pd,axis=1)
    cat_spend_pivot_df = cat_spend_aggregates_df.pivot_table(index='customer_number', columns='primary_category', values=['yoy', 'trend'])
    new_cols = ['customer_number']
    for x in zip(cat_spend_pivot_df.columns.get_level_values(0),
                 [str(name) for name in cat_spend_pivot_df.columns.get_level_values(1)]):
        new_cols.append('{}: {}'.format(x[1], x[0]))
    cat_spend_pivot_df = cat_spend_pivot_df.reset_index()
    cat_spend_pivot_df.columns = new_cols
    cat_spend_pivot_df = spark.createDataFrame(cat_spend_pivot_df)

    #############################
    # Lifetime Savings          #
    #############################
    premium_membership_df = (premium_membership
                                 .withColumn('premium_enrolled',f.lit(1))
                                 .select('customer_number',
                                         'premium_enrolled',
                                         premium_membership.lts.cast('float').alias('lts')))
    premium_membership_df = premium_membership_df.groupBy('customer_number', 'premium_enrolled').agg(
        f.sum('lts').alias('lts'))


    #############################
    # Number of days online     #
    #############################


    #############################################
    # Twelve-month spend                        #
    # Twelve-month number of orders             #
    # Twelve-month number of items purchased    #
    #############################################
    helios_order_header_current = (helios_order_header
                                   .filter(f.col('order_date') < today)
                                   .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR))))

    orders = (helios_order_header_current
        .filter(f.col('order_type') == 'OR')
        .filter(f.col('order_amount') > 0)
        .groupBy('customer_number')
        .agg(
            f.countDistinct('order_number').alias('Twelve-month number of orders'),
            f.sum('order_amount').alias('Twelve-month spend'))
        )

    returns = (helios_order_header_current
               .filter(f.col('order_amount') < 0)
               .groupBy('customer_number')
               .agg(f.countDistinct('order_number').alias('Number of orders with returns'))
               )

    returned_items = (helios_order_line
                      .filter(f.col('order_date') < today)
                      .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
                      .filter(f.col('total_sale_price') < 0)
                      .groupBy('customer_number')
                      .agg(f.count('order_date').alias('Number of returned items'))
                      )

    purchased_items = (helios_order_line
        .filter(f.col('order_date') < today)
        .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
        .filter(f.col('total_sale_price') > 0)
        .groupBy('customer_number')
        .agg(
            f.count('order_date').alias('Number of items purchased'),
            f.countDistinct('staples_sku_number').alias('Number of distinct items purchased'))
        )

    orders_df = (orders
                 .join(returns, ['customer_number'], how='outer')
                 .join(returned_items, ['customer_number'], how='outer')
                 .join(purchased_items, ['customer_number'], how='outer'))

    orders_df = (orders_df
        .fillna(0.0)
        .select(
        'customer_number',
        'Number of returned items',
        'Twelve-month number of orders',
        'Number of items purchased',
        'Number of distinct items purchased',
        'Twelve-month spend',
        return_ratio_udf('Number of orders with returns', 'Twelve-month number of orders').alias('Percent of orders with returns')
    ))

    #############################
    # Purchase trend            #
    #############################
    purchase_cadence_df = cadence_statistics(
        helios_order_header.where(f.col('order_type') == 'OR').where(f.col('order_amount') > 0),
        today - timedelta(2 * ONE_YEAR), today, 'order_date', 'customer_number', name='purchase')

    days_since_last_order_df = days_since_event(
        helios_order_header.where(f.col('order_type') == 'OR').where(f.col('order_amount') > 0),
        today, 'order_date', 'customer_number', 'order')


    #############################
    # Merge tables              #
    #############################

    mm_customer_list = (customer_age_df
                        .join(cat_spend_pivot_df, 'customer_number', 'outer')
                        .join(orders_df, 'customer_number', 'outer')
                        .join(purchase_cadence_df, 'customer_number', 'outer')
                        .join(days_since_last_order_df, 'customer_number', 'outer')
                        .join(web_activity_agg_df, 'customer_number', 'outer')
                        .join(web_visit_cadence_df, 'customer_number', 'outer')
                        .join(premium_membership_df, 'customer_number', 'outer')
                        .join(total_spend_count_df, 'customer_number', 'outer')
                        )
    mm_customer_list = (mm_customer_list
                        .withColumnRenamed('lts','Lifetime Savings')
                        .withColumn('past_purchase_cadence',
                                    num_std_past_udf('days_since_last_order', 'purchase_cadence_avg', 'purchase_cadence_std'))
                        .withColumnRenamed('purchase_cadence_avg','Average number of days between purchases')
                        .withColumnRenamed('purchase_cadence_std','Variance in average time between orders')
                        .withColumnRenamed('total_online_visits','Total number of online visits')
                        .withColumnRenamed('cart_add_ratio','Cart Add Ratio')
                        .withColumnRenamed('customer_age','Customer Age'))

    #############################
    # Write File                #
    #############################
    if training:
        helios_order_header_future = (helios_order_header
                                  .filter(f.col('order_date') >= today)
                                  .filter(f.col('order_date') < (today + timedelta(ONE_YEAR))))
        orders_future = (helios_order_header_future
        .filter(f.col('order_type') == 'OR')
        .filter(f.col('order_amount')>0)
        .groupBy('customer_number')
        .agg(
        f.countDistinct('order_number').alias('Number of orders future'),
        f.sum('order_amount').alias('Twelve-month spend future')
        ))

        def marking_strategy(current_spend, next_year_spend, num_orders_future, num_orders):
            try:
                return ((next_year_spend < 0.6 * current_spend) or (num_orders_future <= 0.5 * num_orders))
            except:
                return True

        marking_udf = udf(marking_strategy, BooleanType())
        all_orders = (orders_future
                      .join(orders, ['customer_number'], how='outer')
                      .fillna(0.0)
                      .withColumn('target', marking_udf('Twelve-month spend', 'Twelve-month spend future',
                                                        'Number of orders future', 'Twelve-month number of orders')))

        return (mm_customer_list
                .filter(f.col('Twelve-month number of orders')>6)
                .join(all_orders, 'customer_number', 'left'))
    else:
        return mm_customer_list


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--training', action='store_true', default=False,
                        help='flag to indicate this run will extract training data')
    parser.add_argument('--today', type=str)
    parser.add_argument('--config', '-config', type=str, help='config file')
    parser.add_argument('--sample',type=float,default=1.0,help='Proportion of order data to sample (from 0 to 1)')
    args = parser.parse_args()

    if args.training:
        if args.today:
            TODAY = dt.strptime(args.today, '%Y/%m/%d')
        else:
            TODAY = dt.today() - timedelta(ONE_YEAR)
    else:
        TODAY = dt.today()

    if args.config is not None:

        config = ConfigParser()
        config.read(args.config)

        # PROJECT_DIR = config.get('paths', 'project')
        # OUTPUT_DIR = config.get('paths', 'output')
        SPARK_HOME = config.get('paths', 'spark')

        os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
        os.environ["SPARK_HOME"] = SPARK_HOME
        os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
        os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
        os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
        os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
        os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
        os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
        os.environ[
            'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 48 --driver-memory 8g --executor-memory 8g --executor-cores 2 --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'
        sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
        sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
        execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
        #spark.sparkContext.addPyFile(config.get('paths', 'timewindows'))
        from pyspark.sql.functions import udf
        from pyspark.sql import functions as f
        from pyspark.sql.types import *
        from pyspark.sql.window import Window
        from pyspark.sql import SparkSession

        spark.sparkContext.setLogLevel("ERROR")


        helios_order_header = (spark
                                   .table("helios.helios_order_header_stage")
                                   .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        helios_order_line = (spark
                             .table("helios.helios_order_line_stage")
                             .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))
        web_activity = spark.table("helios.customer_web_activity")
        customer_profile = spark.table('helios.helios_customer_profile_stage')
        premium_membership = spark.table('helios.premium_lifetime_savings')
    else:
        from pyspark.sql.functions import udf
        from pyspark.sql import functions as f
        from pyspark.sql.types import *
        from pyspark.sql.window import Window
        from pyspark.sql import SparkSession
        spark = (SparkSession.builder.master("local").appName("Customer Attr Compute")
                 .getOrCreate())
        helios_order_header = (spark.read.format("csv").options(header="true")
                               .load("training_data/atrium_helios_order_header_sample.csv")
                               .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        helios_order_line = (spark.read.format("csv").options(header="true")
                             .load("training_data/atrium_helios_order_line_sample.csv")
                             .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
        customer_profile = (spark.read.format("csv").options(header="true")
                            .load('training_data/atrium_customer_profile_sample.csv'))
        web_activity = (spark.read.format("csv").options(header="true")
                        .load('training_data/atrium_customer_web_activity.csv')
                        .withColumn('dt', f.substring('dt', 0, 10).cast('timestamp')))
        premium_membership = (spark.read.format("csv").options(header="true")
                              .load('training_data/atrium_premium_membership_sample.csv'))
        sku_to_category = (spark.read.format("csv").options(header="true")
                           .load("training_data/sku_to_category.csv"))
    if args.sample<1.0:
        helios_order_header = helios_order_header.sample(False,fraction=args.sample,seed=42)
        web_activity = web_activity.sample(False,fraction=args.sample,seed=42)
        helios_order_line = helios_order_line.sample(False,fraction=args.sample,seed=42)

    cust_attr = customer_attributes(helios_order_header, helios_order_line,
                        customer_profile, web_activity, premium_membership,
                        sku_to_category, training=args.training, today=TODAY)

    if args.training:
        path = 'tmp/customer_attributes_training_{}.csv'.format(str(TODAY)[:10])
    else:
        path = 'tmp/customer_attribute_table.csv'
    cust_attr.show()
    try:
        cust_attr.write.csv(path=path,header="true",mode="overwrite",sep=",")
    except:
        cust_attr.toPandas().to_csv(path,index=False)

    spark.stop()
