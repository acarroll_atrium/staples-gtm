#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create Spark Env
import argparse
from ConfigParser import ConfigParser
from datetime import date, timedelta, datetime as dt
import os
import pandas  as pd
import sys

parser = argparse.ArgumentParser()

parser.add_argument('--training', action='store_true',
                    default=False,
                    help='Flag indicating this run will extract training data, with target column, as a .csv file')
parser.add_argument('--today', type=str, help='Aggregates are computed over 12 month period ending on this day')
parser.add_argument('--config', '-config', type=str, help='config file')

args = parser.parse_args()

ONE_YEAR = 365
if args.training:
    try:
        today = dt.strptime(args.today, '%Y/%m/%d')
    except (ValueError, TypeError) as err:
        today = dt.strptime(str(dt.today())[:10], '%Y-%m-%d') - timedelta(ONE_YEAR)
else:
    today = dt.strptime(str(dt.today())[:10], '%Y-%m-%d')
    
config = ConfigParser()
config.read(args.config)

PROJECT_DIR = config.get('paths', 'project')
OUTPUT_DIR = config.get('paths', 'output')
SPARK_HOME = config.get('paths', 'spark')

os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
os.environ["SPARK_HOME"] = SPARK_HOME
os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
os.environ["PYTHONPATH"] = '%spython'%SPARK_HOME
os.environ["PYTHONPATH"] += os.pathsep + '%spython/build'%SPARK_HOME
os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip'%SPARK_HOME
os.environ['HADOOP_CONF_DIR']='/etc/hadoop/conf'
os.environ['PYSPARK_SUBMIT_ARGS']='--master yarn --deploy-mode client --num-executors 48 --driver-memory 8g --executor-memory 8g --executor-cores 2 pyspark-shell'
sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
spark.sparkContext.setLogLevel("ERROR")

####################
# Reserve for Yue  #
####################


# import sys
# sys.path.append('/home/hadoop/datashdp/shared/RewriteZaphodModel/')
#
# import Models.model_utils
# import PythonUtils.s3_model_management
# from cluster_tools import context_creator
# import pandas
# spark_exec_script = context_creator.spark_executable('2.1.1', 'BN', num_exec=48, exec_mem='8g',
#                                                      exec_cores=2, driver_mem='8g')
#
# execfile(spark_exec_script, globals())

from pyspark.sql.functions import udf
from pyspark.sql import functions as f
from pyspark.sql.types import *
from pyspark.sql.window import Window
from pyspark.sql import SparkSession
from datetime import date, timedelta, datetime as dt

spark.sparkContext.setLogLevel("ERROR")

helios_order_header = (spark
                       .table("helios.helios_order_header_stage")
                       .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))

helios_order_line = (spark
                     .table("helios.helios_order_line_stage")
                     .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))

sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))

# web_activity = spark.table("helios.customer_web_activity").sample(False,0.05, 24)

customer_profile = spark.table('helios.helios_customer_profile_stage')

premium_membership = spark.table('helios.premium_lifetime_savings')


def cadence_statistics(df, start_date, end_date, date_col, partition_col, name=''):
    """

    :param df: pyspark dataframe
    :param start_date: beginning of time
    :param end_date: end of time
    :param name: desired based name for columns
    :param date_col: column to use for dates
    :param partition_col: column to use for groupby
    :return: Average lag between events, standard deviation, and total number of events
   """
    window = (Window
              .partitionBy(partition_col)
              .orderBy(date_col))
    df_filter = (df
                 .filter(df[date_col] >= start_date)
                 .filter(df[date_col] < end_date))
    df_filter = df_filter.select(partition_col, date_col).drop_duplicates()
    df_lagged = (df_filter.withColumn('lag', f.datediff(date_col, f.lag(date_col, 1).over(window))))
    return (df_lagged
        .groupBy(partition_col)
        .agg(
        f.count(date_col).alias(name + '_count'),
        f.mean('lag').alias(name + '_cadence_avg'),
        f.stddev('lag').alias(name + '_cadence_std')
    )
    )

def yoy_pd(row):
    """
    :param total: total current year spend
    :param prior_total: total prior year spend
    :param first_purchase_date: first purchase date in category
    :param today: date to use as today
    :return: total/prior_total if applicable
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    today = row['TODAY']
    first_purchase_date = row['first']
    start_date = today - timedelta(2 * ONE_YEAR)
    try:
        return float(total)/(prior_total+1)
    except:
        return 0


def bt_2_pd(row):
    """
    :param total: current year total
    :param recent: current year recent total
    :param prior_total: prior year total
    :param prior_recent: prior year recent (parallel time period) total
    :param first_purchase: first purchase date
    :param today: date to use as today
    :return: A value of 1 is stable: recent spend is stable with respect to yearly spend AND/OR in line with prior year
    behavior. If insufficient prior year data exists, only return twice ratio of current year to next year.
    If little or no activity, return None.
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    recent = row['current_year_recent_spend']
    prior_recent = row['prior_year_recent_spend']
    today = row['TODAY']
    first_purchase_date = row['first']
    if total==0 or (first_purchase_date is None):
        return 0
    elif prior_total == 0 or prior_recent == 0 or (first_purchase_date > today-timedelta(ONE_YEAR * 2)):
        return 2*float(recent) / total
    else:
        return float(recent*prior_total) / ((prior_recent+1) * (total+1))

# def num_std_past(num, avg, std):
#     try:
#         if num >= avg:
#             return float(num-avg)/std
#         else:
#      return 0.0
#     except:
#         return 0.0
# num_std_past_udf = udf(num_std_past, FloatType())

def days_since_event(df, reference_date, date_col, partition_col, col_label='event'):
    """

    :param df: dataframe
    :param reference_date: a date to reference
    :param date_col: column in df that contains date
    :param partition_col: column containing groupby feature
    :param col_label: Name for output column
    :return: partition_column and date difference between most recent (pre-reference_date) event and reference date.
    """
    df_filter = (df
                 .filter(f.col(date_col) < reference_date))
    return (df_filter
        .groupBy(partition_col)
        .agg(f.max(date_col).alias('most_recent_date'))
        .select(
        partition_col,
        f.datediff(f.lit(reference_date), 'most_recent_date').alias('days_since_last_{}'.format(col_label)))
    )



def yoy_pd(row):
    """
    :param total: total current year spend
    :param prior_total: total prior year spend
    :param first_purchase_date: first purchase date in category
    :param today: date to use as today
    :return: total/prior_total if applicable
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    today = row['TODAY']
    first_purchase_date = row['first']
    start_date = today - timedelta(2 * ONE_YEAR)
    try:
        return float(total)/(prior_total+1)
    except:
        return 0


def bt_2_pd(row):
    """
    :param total: current year total
    :param recent: current year recent total
    :param prior_total: prior year total
    :param prior_recent: prior year recent (parallel time period) total
    :param first_purchase: first purchase date
    :param today: date to use as today
    :return: A value of 1 is stable: recent spend is stable with respect to yearly spend AND/OR in line with prior year
    behavior. If insufficient prior year data exists, only return twice ratio of current year to next year.
    If little or no activity, return None.
    """
    total = row['current_year_spend']
    prior_total = row['prior_year_spend']
    recent = row['current_year_recent_spend']
    prior_recent = row['prior_year_recent_spend']
    today = row['TODAY']
    first_purchase_date = row['first']
    if total==0 or (first_purchase_date is None):
        return 0
    elif prior_total==0 or prior_recent==0 or (first_purchase_date>today-timedelta(ONE_YEAR * 2)):
        return 2*float(recent) / total
    else:
        return float((recent*prior_total)) / ((prior_recent+1) * (total+1))



customer_age = (helios_order_header
                .groupBy('customer_number')
                .agg(f.min('order_date').alias('first_order_date')))

customer_age_df = (customer_age
                   .withColumn('customer_age', f.datediff(f.lit(today), 'first_order_date'))
                   .select('customer_number', 'customer_age'))

mm_customer_list = customer_age_df


#########################
# Web activity cadence  #
#########################
# web_activity_filt = (web_activity
#                      .where(f.col("dt").isNotNull())
#                      .filter(f.col('dt') > today - timedelta(ONE_YEAR))
#                      .filter(f.col('dt') <= today))

# web_activity_dates = (web_activity.select('cust_nmb', 'dt').drop_duplicates())
#
#
# web_visit_cadence_df = (cadence_statistics(web_activity_dates, today - timedelta(ONE_YEAR),
#                                            today, 'dt', 'cust_nmb', name='web_activity')
#                         .withColumnRenamed('cust_nmb', 'customer_number'))
#
# web_visit_cadence_df.show(100)
#
# days_since_last_web_visit_df = (days_since_event(web_activity_dates, today, 'dt', 'cust_nmb', 'web_visit')
#                                 .withColumnRenamed('cust_nmb', 'customer_number'))
#
# days_since_last_web_visit_df.show(100)

# def cart_addition_detector(x):
#     if len(x)>0:
#         for i in x:
#             if i .startswith("12"):
#                 return 1
#     return 0
# fun4 = udf(cart_addition_detector, IntegerType())
#
# web_activity_df = (web_activity.filter(web_activity['dt']>today-timedelta(ONE_YEAR)).filter(web_activity['dt']<today))
#
# web_activity_df = (web_activity_df.where(f.col('event_list_string').like('%,%')).withColumn('event_list_substring', f.regexp_replace('event_list_string',',','|')).where(f.length('event_list_substring')>0))
#
#
#
# web_activity_agg_df = (web_activity_df.groupBy("cust_nmb","user_id","dt").agg(f.collect_list(web_activity_df.event_list_substring).alias("list_of_events")).cache())
#
# web_activity_agg_df = web_activity_agg_df.repartition(1)
#
# web_activity_agg= web_activity_agg_df.withColumn("cart_add_indicator", fun4("list_of_events"))
#
# web_activity_agg = web_activity_agg.groupBy('cust_nmb').agg(f.count('dt').alias('total_online_visits'), f.sum('cart_add_indicator').alias('cart_activities'))
#
# web_activity_agg = web_activity_agg.select('cust_nmb','total_online_visits', (web_activity_agg.cart_activities/web_activity_agg.total_online_visits).alias('cart_add_ratio'))
# web_activity_agg = web_activity_agg.withColumnRenamed('cust_nmb','customer_number')
#
# web_activity_agg.show(10)

# web_activity_agg = (web_activity_totals)
# .select(
# f.col('cust_nmb').alias('customer_number'),
# f.col('total_online_visits').alias('Total number of online visits'),
# (web_activity_totals.cart_activities / web_activity_totals.total_online_visits).alias('Cart Add Ratio')))

# web_activity_agg_df = (web_activity_agg)
#                        .join(web_visit_cadence_df,'customer_number','fullouter'))
#                        .join(days_since_last_web_visit_df,'customer_number','fullouter'))

# web_activity_agg_df.show(100)


# mm_customer_list = mm_customer_list.join(web_activity_agg_df,'customer_number','outer')


#############################
# Lifetime Savings          #
#############################

premium_membership_df = (premium_membership
                         .withColumn('premium_enrolled',f.lit(1))
                         .select('customer_number',
                                 'premium_enrolled',
                                 premium_membership.lts.cast('float').alias('lts')))

premium_membership_df = (premium_membership_df.groupBy('customer_number', 'premium_enrolled')
                         .agg(f.sum('lts').alias('lts')))


mm_customer_list = mm_customer_list.join(premium_membership_df,'customer_number','outer')

#############################################
# Twelve-month spend                        #
# Twelve-month number of orders             #
# Twelve-month number of items purchased    #
#############################################

helios_order_header_current = (helios_order_header
                               .filter(f.col('order_date') < today)
                               .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR))))

orders = (helios_order_header_current
    .filter(f.col('order_type') == 'OR')
    .filter(f.col('order_amount') > 0)
    .groupBy('customer_number')
    .agg(
    f.countDistinct('order_number').alias('number_of_orders'),
    f.sum('order_amount').alias('twelve_month_rolling_sales'))
)

returns = (helios_order_header_current
           .filter(f.col('order_amount') < 0)
           .groupBy('customer_number')
           .agg(f.countDistinct('order_number').alias('number_of_orders_with_returns'))
           )

returned_items = (helios_order_line
                  .filter(f.col('order_date') < today)
                  .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
                  .filter(f.col('total_sale_price') < 0)
                  .groupBy('customer_number')
                  .agg(f.count('order_date').alias('number_of_returned_items'))
                  )


purchased_items = (helios_order_line
    .filter(f.col('order_date') < today)
    .filter(f.col('order_date') >= (today - timedelta(ONE_YEAR)))
    .filter(f.col('total_sale_price') > 0)
    .groupBy('customer_number')
    .agg(
    f.count('order_date').alias('number_of_items_purchased'),
    f.countDistinct('staples_sku_number').alias('number_of_distinct_items_purchased'))
)


orders_df = (orders
             .join(returns, ['customer_number'], how='outer')
             .join(returned_items, ['customer_number'], how='outer')
             .join(purchased_items, ['customer_number'], how='outer')
             .fillna(0.0))


orders_df = (orders_df
             .select(
    'customer_number',
    'number_of_returned_items',
    'number_of_orders',
    'number_of_items_purchased',
    'number_of_distinct_items_purchased',
    'twelve_month_rolling_sales',
    'number_of_orders_with_returns')
             .withColumn('ratio_of_returned_items', f.col('number_of_orders_with_returns') / f.col('number_of_orders'))
             )


mm_customer_list = mm_customer_list.join(orders_df,'customer_number','inner')


#############################
# Purchase trend            #
#############################

purchase_cadence_df = cadence_statistics(
    helios_order_header.where(f.col('order_type') == 'OR').where(f.col('order_amount') > 0),
    today - timedelta(2 * ONE_YEAR), today, 'order_date', 'customer_number', name='purchase')

mm_customer_list = mm_customer_list.join(purchase_cadence_df,'customer_number','outer')

days_since_last_order_df = days_since_event(
    helios_order_header.where(f.col('order_type') == 'OR').where(f.col('order_amount') > 0),
    today, 'order_date', 'customer_number', 'order')

mm_customer_list = mm_customer_list.join(days_since_last_order_df,'customer_number','outer')

mm_customer_list = mm_customer_list.withColumn('pre_past_purchase_cadence', (f.col('days_since_last_order')-f.col('purchase_cadence_avg'))/f.col('purchase_cadence_std'))

mm_customer_list = mm_customer_list.withColumn('past_purchase_cadence', f.when(f.col('pre_past_purchase_cadence')<0,0).otherwise(f.col('pre_past_purchase_cadence')))

############################
# Spend by category pivot  #
############################
spend_by_cat = (helios_order_line
                .join(sku_to_category,
                      [helios_order_line.staples_sku_number == sku_to_category.sku_number])
                .select('customer_number', 'order_number', 'customer_item_no','order_date',
                        'shipped_sku_number','total_sale_price', 'primary_category')
                .filter(f.col('total_sale_price') > 0)
                .dropna(subset=['shipped_sku_number'])
                .fillna({'primary_category': 'OTHER'}))

first_order_by_cat = (spend_by_cat
                      .groupBy('customer_number','primary_category')
                      .agg(f.min('order_date').alias('first')))

prior_total = (spend_by_cat
    .filter(f.col('order_date') >= (today-timedelta(2*ONE_YEAR)))
    .filter(f.col('order_date') < (today-timedelta(ONE_YEAR)))
    .groupBy('customer_number','primary_category')
    .agg(
    f.sum('total_sale_price').alias('prior_year_spend')))

prior_recent = (spend_by_cat
    .filter(f.col('order_date') >= (today-timedelta(1.5*ONE_YEAR)))
    .filter(f.col('order_date') < (today-timedelta(ONE_YEAR)))
    .groupBy('customer_number', 'primary_category')
    .agg(
    f.sum('total_sale_price').alias('prior_year_recent_spend')))

current_total = (spend_by_cat
    .filter(f.col('order_date') >= (today-timedelta(ONE_YEAR)))
    .filter(f.col('order_date') < today)
    .groupBy('customer_number', 'primary_category')
    .agg(
    f.sum('total_sale_price').alias('current_year_spend')))

current_recent = (spend_by_cat
    .filter(f.col('order_date') >= (today-timedelta(0.5*ONE_YEAR)))
    .filter(f.col('order_date') < today)
    .groupBy('customer_number', 'primary_category')
    .agg(
    f.sum('total_sale_price').alias('current_year_recent_spend')))

aggregates = (first_order_by_cat
              .join(prior_total, ['customer_number', 'primary_category'],how='outer')
              .join(prior_recent, ['customer_number', 'primary_category'],how='outer')
              .join(current_total, ['customer_number', 'primary_category'],how='outer')
              .join(current_recent, ['customer_number', 'primary_category'],how='outer')
              .fillna(0.0)
              .withColumn('TODAY', f.lit(today)))

cat_spend_agg_df = aggregates.toPandas()
cat_spend_agg_df['yoy'] = cat_spend_agg_df.apply(yoy_pd,axis=1)
cat_spend_agg_df['bench'] = cat_spend_agg_df.apply(bt_2_pd,axis=1)
cat_spend_pivot = cat_spend_agg_df.pivot_table(index='customer_number', columns='primary_category',values=['yoy', 'bench'])
new_cols=['customer_number']
for x in zip(cat_spend_pivot.columns.get_level_values(0),[str(name) for name in cat_spend_pivot.columns.get_level_values(1)]):
   if x[0]=='yoy':
       new_cols.append('{}_spend_year_over_year'.format(x[1].lower().replace(' ','_')))
   else:
       new_cols.append('{}_spend_trend'.format(x[1].lower().replace(' ','_')))

cat_spend_pivot = cat_spend_pivot.reset_index()
cat_spend_pivot.columns = new_cols

cat_spend_pivot.to_csv('/tmp/cat_spend_2019_07_16.csv',index=False)

os.system("hadoop fs -put -f /tmp/cat_spend_2019_07_16.csv")

cat_spend_pivot_df = spark.read.format("csv").option("header", "true").option("inferSchema","true").load("/tmp/cat_spend_2019_07_16.csv")

mm_customer_list = mm_customer_list.join(cat_spend_pivot_df,'customer_number','left')

no_categories = current_total.groupBy('customer_number').agg(f.count('primary_category').alias('no_of_categories_purchased'))

mm_customer_list = mm_customer_list.join(no_categories, 'customer_number','left')

#############################
# Extract Training          #
#############################
if args.training:
    helios_order_header_future = (helios_order_header
                                  .filter(f.col('order_date') >= today)
                                  .filter(f.col('order_date') < (today + timedelta(ONE_YEAR))))

    orders_future = (helios_order_header_future
        .filter(f.col('order_type') == 'OR')
        .filter(f.col('order_amount') > 0)
        .groupBy('customer_number')
        .agg(
        f.countDistinct('order_number').alias('future_orders'),
        f.sum('order_amount').alias('future_spend')))

    all_orders_df = (orders_future.select('customer_number', 'future_orders', 'future_spend')
                     .join(mm_customer_list.select('customer_number', 'number_of_orders', 'twelve_month_rolling_sales'),
                           ['customer_number'], how='outer')
                     .withColumn('sales_ratio', f.col('future_spend') / f.col('twelve_month_rolling_sales'))
                     .withColumn('orders_ratio', f.col('future_orders') / f.col('number_of_orders'))
                     .withColumn('target',
                                 f.when((f.col('sales_ratio') >= 0.6) | (f.col('orders_ratio') >= 0.5), 0).otherwise(1))
                     .fillna(0.0)
                     .select('customer_number', 'future_orders', 'future_spend', 'target'))

    mm_customer_list = (mm_customer_list
                        .filter(f.col('number_of_orders') > 6)
                        .join(all_orders_df, 'customer_number', 'left'))

    mm_customer_list_split = mm_customer_list.randomSplit([0.6, 0.4])

    mm_customer_list_split[0].toPandas().to_csv('/tmp/customer_churn_attributes_train_{}.csv'.format(str(today)[:10]))
    mm_customer_list_split[1].toPandas().to_csv('/tmp/customer_churn_attributes_test_{}.csv'.format(str(today)[:10]))

mm_customer_list = (mm_customer_list
                    .withColumnRenamed('ship_&_pack_spend_trend','ship_pack_spend_trend')
                    .withColumnRenamed('ship_&_pack_spend_year_over_year','ship_pack_year_over_year')
                    .withColumnRenamed('breakroom_spend_year_over_year','breakroom_spend_yoy')
                    .withColumnRenamed('office_supplies_spend_year_over_year','office_supplies_spend_yoy')
                    .withColumnRenamed('specialist_tech_spend_year_over_year','specialist_spend_yoy')
                    .withColumnRenamed('generalist_tech_spend_year_over_year','generalist_year_over_year')
                    .withColumnRenamed('project_furniture_spend_year_over_year','furniture_year_over_year')
                    .withColumnRenamed('project_furniture_spend_trend','furniture_spend_trend')
                    .withColumn('cart_add_ratio',f.lit(0.0))
                    .withColumn('total_online_visits',f.lit(0.0))
                    .select('customer_number',
                            'breakroom_spend_trend' ,
                            'breakroom_spend_yoy' ,
                            'generalist_tech_spend_trend' ,
                            'generalist_year_over_year' ,
                            'jansan_spend_trend' ,
                            'jansan_spend_year_over_year' ,
                            'office_supplies_spend_trend' ,
                            'office_supplies_spend_yoy' ,
                            'paper_spend_trend' ,
                            'paper_spend_year_over_year',
                            'print_spend_trend' ,
                            'print_spend_year_over_year' ,
                            'ship_pack_spend_trend' ,
                            'ship_pack_year_over_year' ,
                            'specialist_tech_spend_trend' ,
                            'specialist_spend_yoy' ,
                            'toner_spend_trend' ,
                            'toner_spend_year_over_year' ,
                            'furniture_spend_trend' ,
                            'furniture_year_over_year' ,
                            'no_of_categories_purchased' ,
                            'twelve_month_rolling_sales' ,
                            'days_since_last_order' ,
                            'purchase_cadence_avg' ,
                            'purchase_cadence_std' ,
                            'past_purchase_cadence' ,
                            'number_of_orders' ,
                            'number_of_items_purchased' ,
                            'lts' ,
                            'customer_age' ,
                            'total_online_visits' ,
                            'cart_add_ratio' ,
                            'ratio_of_returned_items'))

for column in mm_customer_list.columns:
    mm_customer_list = mm_customer_list.withColumn(column,f.when(f.isnan(f.col(column)),None).otherwise(f.col(column)))

mm_customer_list.write.mode("overwrite").saveAsTable("hawkeye.sa_churn_data_for_einstein")

spark.stop()

