import argparse
from ConfigParser import ConfigParser
from datetime import date, timedelta, datetime as dt
import os
import pandas  as pd
import sys

# import pyspark
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--config', '-config', type=str, help='config file')
parser.add_argument('--sample',type=float,help='fraction of the order line data to extract, as a float between 0 and 1')
args = parser.parse_args()
if args.config is not None:
    config = ConfigParser()
    config.read(args.config)

    # PROJECT_DIR = config.get('paths', 'project')
    # OUTPUT_DIR = config.get('paths', 'output')
    SPARK_HOME = config.get('paths', 'spark')

    os.environ["JAVA_HOME"] = '/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
    os.environ["SPARK_HOME"] = SPARK_HOME
    os.environ["PYSPARK_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYSPARK_DRIVER_PYTHON"] = '/home/hadoop/datashdp/.linuxbrew/bin/python'
    os.environ["PYTHONPATH"] = '%spython' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/build' % SPARK_HOME
    os.environ["PYTHONPATH"] += os.pathsep + '%spython/lib/py4j-0.10.3-src.zip' % SPARK_HOME
    os.environ['HADOOP_CONF_DIR'] = '/etc/hadoop/conf'
    os.environ[
        'PYSPARK_SUBMIT_ARGS'] = '--master yarn --deploy-mode client --num-executors 1 --driver-memory 8g --executor-memory 8g --executor-cores 2 --packages com.databricks:spark-csv_2.10:1.4.0 pyspark-shell'
    sys.path.insert(0, os.path.join(SPARK_HOME + "python"))
    sys.path.insert(0, os.path.join(SPARK_HOME + "python/lib/py4j-0.10.3-src.zip"))
    execfile(os.path.join(SPARK_HOME, 'python/pyspark/shell.py'))
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark.sparkContext.setLogLevel("ERROR")
    helios_order_header = (spark
                           .table("helios.helios_order_header_stage")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark
                        .table("helios.helios_order_line_stage")
                        .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    sku_to_category = (spark.table("hawkeye.hawkeye_product_sku_category"))
    web_activity = spark.table("helios.customer_web_activity")
    customer_profile = spark.table('helios.helios_customer_profile_stage')
    premium_membership = spark.table('helios.premium_lifetime_savings')
else:
    from pyspark.sql.functions import udf
    from pyspark.sql import functions as f
    from pyspark.sql.types import *
    from pyspark.sql.window import Window
    from pyspark.sql import SparkSession
    spark = (SparkSession.builder.master("local").appName("Customer Attr Compute")
             .getOrCreate())
    helios_order_header = (spark.read.format("csv").options(header="true")
                           .load("training_data/atrium_helios_order_header_sample.csv")
                           .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    helios_order_line = (spark.read.format("csv").options(header="true")
                         .load("training_data/atrium_helios_order_line_fixed.csv")
                         .withColumn('order_date', f.substring('order_date', 0, 10).cast('timestamp')))
    customer_profile = (spark.read.format("csv").options(header="true")
                        .load('training_data/atrium_customer_profile_sample.csv'))
    web_activity = (spark.read.format("csv").options(header="true")
                    .load('training_data/atrium_customer_web_activity.csv')
                    .withColumn('dt', f.substring('dt', 0, 10).cast('timestamp')))
    premium_membership = (spark.read.format("csv").options(header="true")
                          .load('training_data/atrium_premium_membership_sample.csv'))
    sku_to_category = (spark.read.format("csv").options(header="true")
                       .load("training_data/sku_to_category.csv"))

if args.sample is not None:
    helios_order_line = helios_order_line.sample(False,fraction=args.sample,seed=42)
    web_activity = web_activity.sample(False,fraction=args.sample,seed=42)



ONE_YEAR = 365
TODAY = dt.strptime('2018/05/13','%Y/%m/%d')
today = TODAY

def days_since_event(df, reference_date, date_col, partition_col, col_label='event'):
    """

    :param df: dataframe
    :param reference_date: a date to reference
    :param date_col: column in df that contains date
    :param partition_col: column containing groupby feature
    :param col_label: Name for output column
    :return: partition_column and date difference between most recent (pre-reference_date) event and reference date.
    """
    df_filter = (df
                 .filter(f.col(date_col) < reference_date))
    return (df_filter
        .groupBy(partition_col)
        .agg(f.max(date_col).alias('most_recent_date'))
        .select(
        partition_col,
        f.datediff(f.lit(reference_date), 'most_recent_date').alias('days_since_last_{}'.format(col_label)))
    )

def cadence_statistics(df, start_date, end_date, date_col, partition_col, name=''):
    """

    :param df: pyspark dataframe
    :param start_date: beginning of time
    :param end_date: end of time
    :param name: desired based name for columns
    :param keyword_dict: should include date column and partition column to indicate which columns of df are for dates
    and partitioning
    :return: Average lag between events, standard deviation, and total number of events
    """
    window = (Window
              .partitionBy(partition_col)
              .orderBy(date_col))
    df_filter = (df
                 .filter(df[date_col] >= start_date)
                 .filter(df[date_col] < end_date))
    df_filter = df_filter.select(partition_col, date_col).drop_duplicates()
    df_lagged = (df_filter.withColumn('lag', f.datediff(date_col, f.lag(date_col, 1).over(window))))
    return (df_lagged
            .groupBy(partition_col)
            .agg(
                f.count(date_col).alias(name + '_count'),
                f.mean('lag').alias(name + '_cadence_avg'),
                f.stddev('lag').alias(name + '_cadence_std')))

def cart_add_detector(code):
    if len(code)>0:
        if code.startswith("12"):
            return 1
    return 0

cart_add_udf = udf(cart_add_detector,IntegerType())

web_activity_agg = (web_activity
                            .filter(f.col('dt') > today - timedelta(ONE_YEAR))
                            .filter(f.col('dt') <= today))

# Web activity cadence
web_activity_dates = (web_activity.select('cust_nmb', 'dt').drop_duplicates())

web_visit_cadence_df = (cadence_statistics(web_activity_dates,
                                           today - timedelta(ONE_YEAR),
                                           today,
                                           date_col='dt',
                                           partition_col='cust_nmb',
                                           name='web_activity')
                        .withColumnRenamed('cust_nmb', 'customer_number'))

days_since_last_web_visit_df = (days_since_event(web_activity_dates, today, 'dt', 'cust_nmb', 'web_visit')
                                .withColumnRenamed('cust_nmb', 'customer_number'))

web_activity_totals = (web_activity_agg
    .withColumn('cart_add_indicator',cart_add_udf('event_list_string'))
    .groupBy("cust_nmb")
    .agg(
        f.count('dt').alias('total_online_visits'),
        f.sum('cart_add_indicator').alias('cart_activities')
    )
)

web_activity_agg_df = (web_activity_totals
                       .select(
    f.col('cust_nmb').alias('customer_number'),
    'total_online_visits',
    (f.col('cart_activities').cast(FloatType()) / f.col('total_online_visits').cast(FloatType())).alias('cart_add_ratio')))


(web_activity_agg_df
 .join(web_visit_cadence_df,'customer_number','fullouter')
 .join(days_since_last_web_visit_df,'customer_number','fullouter')
 .toPandas()
 .to_csv('tmp/web_activity_june_27.csv'))