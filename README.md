# Staples GTM

Contains churn_run.py files for testing then promotion to production.

To run script (assuming config file named config exists):
```
python churn_run_ATRIUM_DRAFT.py --config=config
```

In order to generate a training dataset:
```
python churn_run_ATRIUM_DRAFT.py --config=config --training
```

By default, the above run sets the end-date for activities at TODAY minus ONE YEAR. To speficy an end-date:
```
python churn_run_ATRIUM_DRAFT.py --config=config --training --today="YYYY/MM/DD"
```

Finally, for testing, to subsample the tables: 
```
python churn_run_ATRIUM_DRAFT.py --config=config --training --today="YYYY/MM/DD" --sample
```